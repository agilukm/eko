<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/admin/dashboard';
    protected function redirectPath()
    {
        if (Auth::user()->roles==1) {
            return '/admin/dashboard';
        }   else {
            return '/page';
        }
    }

    public function authenticated( \Illuminate\Http\Request $request, \App\User $user ) {
        return redirect()->intended($this->redirectPath())->with('message', 'Berhasil Login');
    }

    public function sendFailedLoginResponse()
    {
        return redirect()->back()
            ->with('error', 'Gagal Login');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


}
