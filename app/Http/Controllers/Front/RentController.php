<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Services\Packages\PackageService;
use App\Services\Rents\RentServiceFront;
use App\Http\Requests\ServeInputRequest;
use Unlu\Laravel\Api\RequestCreator;
use App\User;

use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class RentController extends Controller
{
    protected $service;
    protected $serve_service;
    protected $package_service;

    public function __construct(RentServiceFront $service, ServeService $serve_service, PackageService $package_service)
    {
        $this->service = $service;
        $this->package_service = $package_service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $_SERVER['PATH_INFO'] = '';
        $req = RequestCreator::createWithParameters([
            'user_id' => Auth::user()->id
        ]);

        $data = array(
            'serves' => $this->serve_service->browse($request),
            'packages' => $this->package_service->browse($request),
            'invalidDate' => $this->service->invalidDate($request),
            'data' => $this->service->browse($req)
        );
        return View('front.rent', $data);
    }

    public function invoice($id)
    {
        $data = array(
            'data' => $this->service->read($id)
        );
        return View('invoice', $data);
    }

    public function tambah(Request $req)
    {
        $_SERVER['PATH_INFO'] = '';
        $request = RequestCreator::createWithParameters([
            'id' => '!= 9999999999'
        ]);
        $data = array(
            'serves' => $this->serve_service->browse($request),
            'packages' => $this->package_service->browse($request),
            'invalidDate' => $this->service->invalidDate($request),
            'users' => User::where('roles',0)->where('owner',0)->get()
        );
        return View('admin.rents.add', $data);
    }

    public function add(Request $request)
    {
        $add = $this->service->add($request->toArray());
        return redirect('page/pembayaran/'.$add)->with('message', 'Berhasil Disimpan. Silahkan melakukan konfirmasi pembayaran');
    }

    public function edit(Request $request, $id)
    {
        $this->service->edit($request->toArray(), $id);
        return redirect('admin/sewa')->with('message', 'Berhasil Disimpan');
    }

    public function read(Request $request, $id)
    {
        $data = array(
            'data' => $this->service->read($id),
            'serves' => $this->serve_service->browse($request),
            'packages' => $this->package_service->browse($request),
            'invalidDate' => $this->service->invalidDateId($request,$id),
            'users' => User::where('roles',0)->where('owner',0)->get()
        );
        return View('admin.rents.read', $data);
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }
}
