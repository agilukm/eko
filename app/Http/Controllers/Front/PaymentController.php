<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Services\Packages\PackageService;
use App\Services\Rents\RentService;
use App\Services\Payments\PaymentServiceFront;
use App\Http\Requests\ServeInputRequest;
use App\User;

use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class PaymentController extends Controller
{
    protected $service;
    protected $payment;
    protected $serve_service;
    protected $package_service;

    public function __construct(PaymentServiceFront $payment, RentService $service, ServeService $serve_service, PackageService $package_service)
    {
        $this->payment = $payment;
        $this->service = $service;
        $this->package_service = $package_service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'data' => $this->payment->browse($request),
        );
        return View('front.payment', $data);

    }

    public function read($id)
    {
        $data = array(
            'data' => $this->payment->read($id)
        );
        return View('front.payment_details', $data);
    }

    public function upload($id, Request $request)
    {
        if ($this->payment->upload($id, $request)) {
            return redirect('page/pembayaran')->with('message', 'Berhasil Disimpan, menunggu konfirmasi');
        }
        return back()->with('message', 'Gagal Disimpan, silahkan coba lagi');
    }

    public function tambah($value='')
    {
        $data = array(
            'delay' => $this->payment->list_delay(),
         );
        return View('admin.payments.add', $data);
    }

    public function add(Request $request)
    {
        if ($this->payment->add($request)) {
            return redirect('admin/pembayaran')->with('message', 'Berhasil Disimpan');
        }
    }

    public function detail_bayar($id)
    {
        return $this->payment->detail_bayar($id);
    }

    public function delete($id)
    {
        $this->payment->delete($id);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }

    public function expired($id)
    {
        $this->payment->expired($id,8);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }
}
