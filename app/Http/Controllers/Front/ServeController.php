<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Http\Requests\ServeInputRequest;
use App\Services\Rents\RentServiceFront;
use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class ServeController extends Controller
{
    protected $service;
    protected $rent_service;

    public function __construct(ServeService $service, RentServiceFront $rent_service)
    {
        $this->service = $service;
        $this->rent_service = $rent_service;
        //$this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'serves' => $this->service->browse($request),
            'data' => $this->service->browse($request),
            'invalidDate' => $this->rent_service->invalidDate($request),
        );
        return View('front.serve', $data);
    }

    public function addView(ServeInputRequest $request)
    {
        return View('admin.serves.add');
    }

    public function add(ServeInputRequest $request)
    {
        $this->service->add($request);
        return redirect('admin/jasa')->with('message', 'Berhasil Disimpan');;
    }

    public function edit(Request $request, $id)
    {
        $this->service->edit($request, $id);
        return redirect('admin/jasa')->with('message', 'Berhasil Disimpan');
    }

    public function read(Request $request, $id)
    {
        $data = array(
            'data' => $this->service->read($id)
        );
        return View('front.serve_detail', $data);
    }

    public function read_ajax(Request $request, $id)
    {
        return $this->service->read($id);
    }


    public function delete($id)
    {
        $this->service->delete($id);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }
}
