<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Services\Packages\PackageService;
use App\Services\Rents\RentService;
use App\Http\Requests\ServeInputRequest;
use App\User;

use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class ScheduleController extends Controller
{
    protected $service;
    protected $serve_service;
    protected $package_service;

    public function __construct(RentService $service, ServeService $serve_service, PackageService $package_service)
    {
        $this->service = $service;
        $this->package_service = $package_service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'data' => $this->service->browse($request),
            'invalidDate' => $this->service->invalidDate($request),
        );
        return View('admin.schedule.index', $data);
    }    
}
