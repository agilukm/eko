<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Packages\PackageService;
use App\Services\Serves\ServeService;
use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class PackageController extends Controller
{
    protected $service;
    protected $serve_service;

    public function __construct(PackageService $service, ServeService $serve_service)
    {
        $this->service = $service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'menu' => 'package',
            'data' => $this->service->browse($request)
        );
        return View('admin.packages.index', $data);
    }

    public function read(Request $request, $id)
    {
        $package = $this->service->read($id);
        // dd($package->details->pluck('id')->toArray());
        $data = array(
            'serves' => $this->serve_service->browse($request),
            'package' => $this->service->read($id)
        );
        return View('admin.packages.edit',$data);
    }

    public function edit(Request $request, $id)
    {
        $this->service->edit($request->toArray(), $id);
        return redirect('admin/paket')->with('message', 'Berhasil Disimpan');
    }

    public function add(Request $request)
    {
        $this->service->add($request->toArray());
        return redirect('admin/paket')->with('message', 'Berhasil Disimpan');
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }

    public function tambah(Request $request)
    {
        $list = $this->serve_service->browse($request);
        $data = array(
            'serves' => $list,
        );
        return View('admin.packages.add', $data);
    }

    public function details(Request $request,$id)
    {
        $details = $this->service->details_json($request, $id);
        return $details;
    }
}
