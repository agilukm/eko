<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Payments\Payment;
use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;
use DB;
use Carbon\Carbon as cc;

class adminController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        $total_rent = DB::table('rents')->where('status', '!=', 8)->sum('total');
        $total_discount = DB::table('rents')->where('status', '!=', 8)->sum('discount');
        $data = array(
            'penyewaan' => number_format($total_rent-$total_discount),
            'jasa' => DB::table('serves')->count('id'),
            'paket' => DB::table('packages')->count('id'),
            'pengguna' => DB::table('users')->count('id'),
            'events' => \App\Services\Rents\Rent::where('date', '>', cc::now())->get()
        );
        if (!Auth::check() || Auth::user()->roles==0) {
            return redirect('page');
        }
        return View('admin.dashboard',$data);
    }
    public function email(Request $request)
    {
        $payment = Payment::first();
        $arrayName = array(
            'headline' => 'Selamat',
            'title' => 'Pembayaran Diterima',
            'data' => $payment
        );
        // dd($arrayName);
        Mail::send('test', $arrayName , function ($message) use ($request)
        {
            $message->subject('a');
            $message->from('skripsianunikom@gmail.com', 'Admin');
            $message->to('agilukm07@gmail.com');
        });
    }

}
