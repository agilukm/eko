<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Services\Packages\PackageService;
use App\Services\Rents\RentService;
use App\Http\Requests\ServeInputRequest;
use Unlu\Laravel\Api\RequestCreator;
use App\User;

use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;
use Excel;
use DB;

class ReportController extends Controller
{
    protected $service;
    protected $serve_service;
    protected $package_service;

    public function __construct(RentService $service, ServeService $serve_service, PackageService $package_service)
    {
        $this->service = $service;
        $this->package_service = $package_service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'data' => $this->service->browse($request)
        );
        return View('admin.reports.index', $data);
    }

    public function export_rent(Request $request)
    {
        $_SERVER['PATH_INFO'] = '';
        Excel::create('Laporan Penyewaan', function($excel) use ($request) {
            $excel->sheet('Laporan_Penyewaan', function($sheet)  use ($request) {
                $sheet->setAutoSize(true);

                $data = array(
                    'rent' => \App\Services\Rents\Rent::where('status', '!=', 8)->whereBetween('date',array($request->dari,$request->sampai))->get() ,
                    'dari' => $request->dari ,
                    'sampai' => $request->sampai ,
                    'discount' => DB::table('rents')->where('status', '!=', 8)->whereBetween('date',array($request->dari,$request->sampai))->sum('discount'),
                    'total' => DB::table('rents')->where('status', '!=', 8)->whereBetween('date',array($request->dari,$request->sampai))->sum('total')
                );

                $sheet->loadView('admin.reports.export_rent', $data);
            });
        })->download('xls');
    }

    public function export_serve(Request $request)
    {
        $_SERVER['PATH_INFO'] = '';
        Excel::create('Laporan Jasa', function($excel) use ($request) {
            $excel->sheet('Laporan_Jasa', function($sheet)  use ($request) {
                $sheet->setAutoSize(true);
                $data = array(
                    'serves' => \App\Services\Serves\Serve::get(),
                );
                $sheet->loadView('admin.reports.export_serve', $data);
            });
        })->download('xls');
    }
    public function export_package(Request $request)
    {
        $_SERVER['PATH_INFO'] = '';
        Excel::create('Laporan Paket', function($excel) use ($request) {
            $excel->sheet('Laporan_Paket', function($sheet)  use ($request) {
                $sheet->setAutoSize(true);
                $data = array(
                    'packages' => \App\Services\Packages\Package::get(),
                );
                $sheet->loadView('admin.reports.export_package', $data);
            });
        })->download('xls');
    }

}
