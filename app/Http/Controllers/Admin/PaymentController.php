<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Services\Serves\ServeService;
use App\Services\Packages\PackageService;
use App\Services\Rents\RentService;
use App\Services\Payments\PaymentService;
use App\Http\Requests\ServeInputRequest;
use App\User;

use View;
use Auth;
use Redirect;
use Carbon;
use Session;
use Mail;
use Response;

class PaymentController extends Controller
{
    protected $service;
    protected $payment;
    protected $serve_service;
    protected $package_service;

    public function __construct(PaymentService $payment, RentService $service, ServeService $serve_service, PackageService $package_service)
    {
        $this->payment = $payment;
        $this->service = $service;
        $this->package_service = $package_service;
        $this->serve_service = $serve_service;
        $this->middleware('auth');
    }

    public function browse(Request $request)
    {
        $data = array(
            'data' => $this->payment->browse($request),
        );
        return View('admin.payments.index', $data);

    }

    public function process($id, $status)
    {
        if ($this->payment->process($id, $status)) {
            return redirect('admin/pembayaran')->with('message', 'Berhasil Disimpan');
        }
    }

    public function tambah($value='')
    {
        $data = array(
            'delay' => $this->payment->list_delay(),
         );
        return View('admin.payments.add', $data);
    }

    public function add(Request $request)
    {
        if ($this->payment->add($request)) {
            return redirect('admin/pembayaran')->with('message', 'Berhasil Disimpan');
        }
    }

    public function detail_bayar($id)
    {
        return $this->payment->detail_bayar($id);
    }

    public function delete($id)
    {
        $this->payment->delete($id);
        return response('success', 200)
                 ->header('Content-Type', 'text/plain');
    }
}
