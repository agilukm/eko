<?php

namespace App\Http\Middleware;

use Closure;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        public function handle($request, Closure $next)
        {
            if (Auth::check()) {
                if(auth()->user()->roles == 1){
                    return $next($request);
                }
            }
            return redirect('/')->with('error','You have not owrner access');
        }
}
