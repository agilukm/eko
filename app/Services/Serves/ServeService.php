<?php

namespace App\Services\Serves;

use QueryBuilder;
use Storage;
class ServeService
{

    private $serve;
    private $queryBuilder;

    function __construct(Serve $serve)
    {
        $this->serve = $serve;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->serve, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->serve->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $serve = $this->serve->findOrFail($id);
        $input['fileurl'] = $serve->fileurl;
        $input['type'] = $serve->type;
        if ($file = $input->file('file')) {
            if ($file->getClientOriginalName() && $this->uploadfile($input->file('file'))) {
                $input['fileurl'] = Storage::url('serve/'.$file->getClientOriginalName());
                $input['type'] = 'file';
            }
        }
        //
        $data = $this->fillServe($input);
        $serve->fill($data->toArray());
        $serve->save();
        return $serve;
    }

    public function add($input)
    {
        $input['fileurl'] = '';
        $input['type'] = '';
        if ($file = $input->file('file')) {
            $this->uploadfile($input->file('file'));
            $input['fileurl'] = Storage::url('serve/'.$file->getClientOriginalName());
            $input['type'] = 'file';
        }
        // dd($input->all());
        $data = $this->fillServe($input);
        $this->serve->fill($data->toArray());
        $this->serve->save();
        return $this->serve;
    }
    public function uploadfile($file)
    {
        return Storage::disk('local')->put('serve/'.$file->getClientOriginalName(), file_get_contents($file->getRealPath()));
    }
    public function delete($id)
    {
        return $this->serve->destroy($id);
    }

    public function fillServe($data)
    {
        $this->serve->name = $data['name'];
        $this->serve->price = $data['price'];
        $this->serve->unit = $data['unit'];
        $this->serve->fileurl = $data['fileurl'];
        $this->serve->type = $data['type'];
        $this->serve->stock = $data['stock'];
        $this->serve->min = $data['min'];
        $this->serve->description = $data['description'];
        return $this->serve;
    }

}
