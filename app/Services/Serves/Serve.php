<?php

namespace App\Services\Serves;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Serve extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'price', 'unit', 'fileurl', 'type', 'description', 'stock', 'min'
    ];

    protected $date = [
        'deleted_at'
    ];

}
