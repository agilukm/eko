<?php

namespace App\Services\Rents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentDetails extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'rent_id', 'serve_id', 'qty', 'total'
    ];

    protected $date = [
        'deleted_at'
    ];

    public function rent()
    {
        return $this->belongsTo(Rent::class, 'rent_id');
    }

    public function serves()
    {
        return $this->belongsTo(\App\Services\Serves\Serve::class, 'serve_id');
    }

}
