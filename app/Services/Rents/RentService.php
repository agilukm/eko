<?php

namespace App\Services\Rents;

use QueryBuilder;
use App\Services\Payments\Payment;

class RentService
{

    private $rent;
    private $rent_details;
    private $queryBuilder;

    function __construct(Rent $rent, RentDetails $rent_details, Payment $payment)
    {
        $this->rent = $rent;
        $this->payment = $payment;
        $this->rent_details = $rent_details;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->rent, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->rent->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $rent = $this->rent->findOrFail($id);
        $rent->details()->delete();
        $data = $this->fillRent($input);
        $rent->fill($data->toArray());
        $rent->save();
        $id = $rent->id;
        foreach ($input['serve'] as $key => $serve) {
            $this->rent_details->create([
                'rent_id'=> $id,
                'serve_id' => $serve,
                'qty' => $input['qty'][$key],
                'total' => $input['total'][$key]
            ]);
        }

        return $rent;
    }

    public function add($input)
    {
        $data = $this->fillRent($input);
        $this->rent->fill($data->toArray());
        $maxcode = $this->rent->count();
        $this->rent->code = 'S'.++$maxcode;
        $this->rent->save();
        $id = $this->rent->id;

        foreach ($input['serve'] as $key => $serve) {
            $this->rent_details->create([
                'rent_id'=> $id,
                'serve_id' => $serve,
                'qty' => $input['qty'][$key],
                'total' => $input['total'][$key]
            ]);
        }


        $type = 'Tunai';
        $total = $input['final'];
        if ($data['type'] == 'Paket') {
            if ($input['payment_type'] == 'Dp') {
                $total = $input['dp'];
                $type = $input['payment_type'];
            }
        }

        $maxcodepayment = $this->payment->count();
        $payment = [
                'rent_id' => $id,
                'total' => $total,
                'file' => '',
                'status' => 3,
                'type' => $type,
                'code' => 'B'.++$maxcodepayment
        ];

        $this->payment->fill($payment)->save();

        return $this->rent;
    }

    public function invalidDate($value='')
    {
        return $this->rent->all();
    }

    public function invalidDateId($value='',$id)
    {
        return $this->rent->where('id','!=',$id)->get();
    }

    public function delete($id)
    {
        $rent = $this->rent->findOrFail($id);
        $rent->details()->delete();
        return $rent->delete();
    }

    public function fillRent($data)
    {
        $this->rent->user_id = $data['user_id'];
        $this->rent->date = $data['date'];
        $this->rent->type = $data['type'];
        $this->rent->total = $data['final'];
        $this->rent->payment_type = 'Tunai';
        $this->rent->status = 4;
        if ($data['type'] == 'Paket') {
            $this->rent->source_id = $data['package'];
            $this->rent->payment_type = $data['payment_type'];
            if ($data['payment_type'] == 'Dp') {
                $this->rent->status = 3;
            }
        }
        return $this->rent;
    }

}
