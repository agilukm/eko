<?php

namespace App\Services\Rents;

use QueryBuilder;
use App\Services\Payments\Payment;

class RentServiceFront
{

    private $rent;
    private $rent_details;
    private $queryBuilder;

    function __construct(Rent $rent, RentDetails $rent_details, Payment $payment)
    {
        $this->rent = $rent;
        $this->payment = $payment;
        $this->rent_details = $rent_details;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->rent, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->rent->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $rent = $this->rent->findOrFail($id);
        $rent->details()->delete();
        $data = $this->fillRent($input);
        $rent->fill($data->toArray());
        $rent->save();
        $id = $rent->id;
        foreach ($input['serve'] as $key => $serve) {
            $this->rent_details->create([
                'rent_id'=> $id,
                'serve_id' => $serve,
                'qty' => $input['qty'][$key],
                'total' => $input['total'][$key]
            ]);
        }

        return $rent;
    }

    public function add($input)
    {
        $input['discount'] = $this->discount($input['user_id'],$input['final']);
        $data = $this->fillRent($input);
        $this->rent->fill($data->toArray());
        $maxcode = $this->rent->count();
        $this->rent->code = 'S'.++$maxcode;
        $this->rent->save();
        $id = $this->rent->id;

        foreach ($input['serve'] as $key => $serve) {
            $this->rent_details->create([
                'rent_id'=> $id,
                'serve_id' => $serve,
                'qty' => $input['qty'][$key],
                'total' => $input['total'][$key]
            ]);
        }
        $type = 'Tunai';
        $total = $input['final']-$input['discount'];
        if ($data['type'] == 'Paket') {
            if ($input['payment_type'] == 'Dp') {
                $total = $input['dp'];
                $type = 'Dp';
            }
        }

        $expired_at = date('Y-m-d H:i:s', strtotime("+05 minutes", strtotime(date("Y-m-d H:i:s"))));
        $maxcodepayment = $this->payment->count();
        $payment = [
                'rent_id' => $id,
                'total' => $total,
                'file' => '',
                'status' => 3,
                'type' => $type,
                'code' => 'B'.++$maxcodepayment,
                'expired_at' => $expired_at
        ];

        $this->payment->fill($payment)->save();

        return $this->payment->id;
    }

    public function discount($user_id,$total)
    {
        if ($this->rent->where('user_id', $user_id)->whereBetween('status', [4, 6])->first()) {
            $discount = $total*10/100;
            return $discount;
        }
        return '0';
    }

    public function invalidDate($value='')
    {
        return $this->rent->where('status','!=','8')->get();
    }

    public function invalidDateId($value='',$id)
    {
        return $this->rent->where('id','!=', $id)->get();
    }

    public function delete($id)
    {
        $rent = $this->rent->findOrFail($id);
        $rent->details()->delete();
        return $rent->delete();
    }

    public function fillRent($data)
    {
        $this->rent->user_id = $data['user_id'];
        $this->rent->date = $data['date'].' '.$data['time'];
        $this->rent->type = $data['type'];
        $this->rent->total = $data['final'];
        $this->rent->payment_type = 'Tunai';
        $this->rent->discount = $data['discount'];
        $this->rent->address = $data['address'];
        $this->rent->status = 0;
        if ($data['type'] == 'Paket') {
            $this->rent->source_id = $data['package'];
            $this->rent->payment_type = $data['payment_type'];
            if ($data['payment_type'] == 'Dp') {
                $this->rent->status = 0;
            }
        }
        return $this->rent;
    }

}
