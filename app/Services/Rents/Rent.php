<?php

namespace App\Services\Rents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Rent extends Model
{
    use SoftDeletes;

    const STATUS_NEW = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_WAIT = 3;
    const STATUS_PAID = 4;
    const STATUS_PROCESS = 5;
    const STATUS_DONE = 6;
    const STATUS_VERIFICATION = 7;
    const STATUS_FAILED = 8;

    public static function listStatus()
    {
        return [
            self::STATUS_NEW    => 'Menunggu Pembayaran',
            self::STATUS_ACCEPTED => 'Diterima',
            self::STATUS_REJECTED  => 'Ditolak',
            self::STATUS_WAIT  => 'Menunggu Pelunasan',
            self::STATUS_PAID  => 'Lunas, Menunggu Pelaksanaan',
            self::STATUS_PROCESS  => 'Dalam Proses',
            self::STATUS_DONE  => 'Selesai',
            self::STATUS_VERIFICATION  => 'Menunggu Konfirmasi Pembayaran',
            self::STATUS_FAILED  => 'Pemesanan Dibatalkan, Pelunasan tidak dilakukan'
        ];
   }

   /**
    * Returns label of actual status

    * @param string
    */
   public function statusLabel()
   {
       $list = self::listStatus();
       return isset($list[$this->status]) ? $list[$this->status] : $this->status;
   }

    protected $fillable = [
        'user_id', 'date', 'type', 'total', 'status', 'source_id', 'payment_type', 'discount', 'code', 'address'
    ];

    protected $date = [
        'deleted_at'
    ];

    public function users()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    public function details()
    {
        return $this->hasMany(RentDetails::class, 'rent_id');
    }

    public function types()
    {
        return $this->belongsTo(\App\Services\Packages\Package::class, 'source_id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] =  Carbon::parse($value);
    }

    public function payments()
    {
        return $this->hasMany(\App\Services\Payments\Payment::class, 'rent_id');
    }
}
