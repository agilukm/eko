<?php

namespace App\Services\Payments;

use App\Services\Rents\Rent;
use QueryBuilder;
use DB;
use Mail;
class PaymentService
{

    private $payment;
    private $rent;
    private $queryBuilder;

    function __construct(Payment $payment, Rent $rent)
    {
        $this->payment = $payment;
        $this->rent = $rent;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->payment, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->payment->findOrFail($id);
    }

    public function process($id, $status)
    {
        $payment = $this->payment->findOrFail($id);

        $data = [
            "status" => $status
        ];

        $payment->fill($data);
        $payment->save();

        if ($payment->type == 'Tunai' && $status == 1) {
            $status_rent = 4;
            $arrayName = array(
                'headline' => 'Selamat',
                'title' => 'Pembayaran Diterima',
                'data' => $payment
            );
            $this->send_email('test', $arrayName , 'Pembayaran Diterima', $payment->rents->users->email);
        }
        elseif ($payment->type == 'Dp' && $status == 1) {
            $status_rent = 3;
            $total = $payment->rents->total - $payment->rents->discount;
            $dp = $payment->total;
            $rest = $total-$dp;
            $expired_at = date('Y-m-d H:i:s', strtotime("+21 days", strtotime(date("Y-m-d H:i:s"))));

            $data_rent = [
                'total' => $rest,
                'file' => '',
                'status' => 3,
                'rent_id' => $payment->rents->id,
                'type' => 'Tunai',
                'code' => 'B'.$this->max_code(),
                'expired_at' => $expired_at,
            ];
            $create = $this->payment->create($data_rent);
            $arrayName = array(
                'headline' => 'Selamat',
                'title' => 'Pembayaran Diterima',
                'data' => $payment,
                'new_payment' => $create
            );
            $this->send_email('test', $arrayName , 'Pembayaran Diterima, menunggu pelunasan', $payment->rents->users->email);
        }
        elseif ($status == 2) {
            $status_rent = 0;
            $this->send_email('test', $payment , 'Pembayaran Ditolak', $payment->rents->users->email);
        }
        $rent = $this->rent->findOrFail($payment->rent_id);
        $data_rent = [
            "status" => $status_rent
        ];
        $rent->fill($data_rent);
        $rent->save();
        return $payment;
    }

    public function send_email($template, $data, $title, $user)
    {

        Mail::send($template, $data , function ($message) use($title, $user)
        {
            $message->subject($title);
            $message->from(env('MAIL_USERNAME','skripsianunikom@gmail.com'), 'Admin');
            $message->to($user);
        });
    }

    public function edit($input, $id)
    {
        $payment = $this->payment->findOrFail($id);
        $data = $this->fillPayment($input);
        $payment->fill($data->toArray());
        $payment->save();

        return $payment;
    }

    public function add($input)
    {
        $data = $this->fillServe($input);
        $this->payment->fill($data->toArray());
        $this->payment->save();

        $list = $this->detail_bayar($this->payment->rent_id);

        if ($list['rest'] == 0) {
            $rent = $this->rent->find($this->payment->rent_id);
            $rent->status = 4;
            $rent->save();
        }

        return $this->payment;
    }

    public function delete($id)
    {
        return $this->payment->destroy($id);
    }

    public function max_code()
    {
        return $this->payment->count()+1;
    }

    public function fillServe($data)
    {
        $this->payment->rent_id = $data['rent_id'];
        $this->payment->total = $data['final'];
        $this->payment->file = '';
        $this->payment->status = 1;
        return $this->payment;
    }

    public function list_delay($value='')
    {
        return $this->rent->where('status','<',4)->get();
    }

    public function detail_bayar($id)
    {
        $payment = $this->rent->find($id);
        $json['total'] = $payment->total;
        $json['paid'] = intval(DB::table('payments')->where('rent_id', $id)->sum('total'));
        $json['rest'] = $json['total']-$json['paid'];
        return $json;

    }

}
