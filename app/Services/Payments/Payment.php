<?php

namespace App\Services\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    const STATUS_NEW = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_WAITING = 3;

    public static function listStatus()
    {
        return [
            self::STATUS_NEW    => 'Menunggu Konfirmasi',
            self::STATUS_ACCEPTED => 'Diterima',
            self::STATUS_REJECTED  => 'Ditolak',
            self::STATUS_WAITING  => 'Menunggu Pembayaran'
        ];
   }

   /**
    * Returns label of actual status

    * @param string
    */
   public function statusLabel()
   {
       $list = self::listStatus();
       return isset($list[$this->status]) ? $list[$this->status] : $this->status;
   }

    protected $fillable = [
        'name', 'rent_id', 'total', 'file', 'status', 'type', 'code', 'expired_at'
    ];

    protected $date = [
        'deleted_at'
    ];

    public function rents()
    {
        return $this->belongsTo(\App\Services\Rents\Rent::class, 'rent_id');
    }
}
