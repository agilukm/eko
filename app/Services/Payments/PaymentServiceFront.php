<?php

namespace App\Services\Payments;

use App\Services\Rents\Rent;
use QueryBuilder;
use DB;
use Storage;
use Auth;

class PaymentServiceFront
{

    private $payment;
    private $rent;
    private $queryBuilder;

    function __construct(Payment $payment, Rent $rent)
    {
        $this->payment = $payment;
        $this->rent = $rent;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->payment, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->payment->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $payment = $this->payment->findOrFail($id);
        $data = $this->fillPayment($input);
        $payment->fill($data->toArray());
        $payment->save();

        return $payment;
    }

    public function expired($payment_id)
    {
        $payment = $this->payment->findOrFail($payment_id);
        $rent = $this->rent->findOrFail($payment->rent_id);
        if ($payment->status == 1 ) {
            return ;
        }
        if ($rent->status >= 4 && $rent->status <=6) {
            return ;
        }
        $rent->status = 8;
        $rent->save();
        $payment->delete();
        return $payment;
    }

    public function upload($id,$input)
    {
        $payment = $this->payment->findOrFail($id);
        $file = $input->file('file');
        if ($this->uploadfile($file)) {
            $status = 0;
            $data = [
                "file" => Storage::url($file->getClientOriginalName()),
                "status" => $status
            ];
            $payment->fill($data);
            $payment->save();
            $status_rent = 7;

            $this->updateRent($payment->rent_id, $status_rent);
            return $payment;
        }
        redirect('page/pembayaran')->with('message', 'Gagal Disimpan, silahkan coba lagi');
    }

    public function uploadfile($file)
    {
        return Storage::disk('local')->put($file->getClientOriginalName(), file_get_contents($file->getRealPath()));
    }

    public function updateRent($id,$status_rent)
    {
        $rent = $this->rent->findOrFail($id);
        $data = [
            "status" => $status_rent
        ];
        $rent->fill($data);
        return $rent->save();
    }

    public function max_code()
    {
        return $this->payment->count()+1;
    }

    public function add($input)
    {
        $data = $this->fillServe($input);
        $this->payment->fill($data->toArray());
        $this->payment->code = 'B'.max_code();
        $this->payment->save();

        $list = $this->detail_bayar($this->payment->rent_id);

        if ($list['rest'] == 0) {
            $rent = $this->rent->find($this->payment->rent_id);
            $rent->status = 4;
            $rent->save();
        }

        return $this->payment;
    }

    public function delete($id)
    {
        return $this->payment->destroy($id);
    }

    public function fillServe($data)
    {
        $this->payment->rent_id = $data['rent_id'];
        $this->payment->total = $data['final'];
        $this->payment->file = '';
        $this->payment->status = 1;
        return $this->payment;
    }

    public function list_delay($value='')
    {
        return $this->rent->where('status','<',4)->get();
    }

    public function detail_bayar($id)
    {
        $payment = $this->rent->find($id);
        $json['total'] = $payment->total;
        $json['paid'] = intval(DB::table('payments')->where('rent_id', $id)->sum('total'));
        $json['rest'] = $json['total']-$json['paid'];
        return $json;

    }

}
