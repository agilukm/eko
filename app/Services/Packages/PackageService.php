<?php

namespace App\Services\Packages;

use QueryBuilder;
use Unlu\Laravel\Api\RequestCreator;

class PackageService
{

    private $package;
    private $package_details;

    function __construct(Package $package, PackageDetails $package_details)
    {
        $this->package = $package;
        $this->package_details = $package_details;
    }

    public function browse($input)
    {
        $query = new QueryBuilder($this->package, $input);
        return $query->build()->get();
    }

    public function read($id)
    {
        return $this->package->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $package = $this->package->findOrFail($id);
        $data = $this->fillPackage($input);
        $package->fill($data->toArray());
        $package->save();
        $package->details()->delete();
        $id = $package->id;
        foreach ($input['serve'] as $key => $serve) {
            $this->package_details->create(['package_id'=> $id, 'serve_id' => $serve, 'total_unit' => $input['jumlah'][$key]]);
        }
        return $package;
    }

    public function details($input, $id)
    {
        $request = RequestCreator::createWithParameters([
              'package_id' => $id,
        ]);
        $query = new QueryBuilder($this->package, $request);
        return $query->build()->get();
    }

    public function details_json($input, $id)
    {
        $data = $this->package->findOrFail($id);
        $data["details"] = $data->details;
        foreach ($data["details"] as $key => $value) {
            $data["details"][$key]["serve"] = $data->details[$key]->serve;
        }
        $array = array('data' => $data, );
        return $array;
    }

    public function add($input)
    {
        $data = $this->fillPackage($input);
        $package = $this->package->fill($data->toArray());
        $package->save();
        $id = $package->id;
        foreach ($input['serve'] as $key => $serve) {
            $this->package_details->create(['package_id'=> $id, 'serve_id' => $serve, 'total_unit' => $input['jumlah'][$key]]);
        }
        return $package;
    }

    public function delete($id)
    {
        $package = $this->package->findOrFail($id);
        $package->details()->delete();
        return $package->delete();
    }

    public function fillPackage($data)
    {
        $this->package->name = $data['name'];
        $this->package->price = $data['price'];
        $this->package->description = $data['description'];
        return $this->package;
    }

    public function fillPackageDetails($package_id, $serve, $unit)
    {
        $this->package_details->package_id = $package_id;
        $this->package_details->serve_id = $serve;
        $this->package_details->total_unit = $unit;
        return $this->package_details;
    }

}
