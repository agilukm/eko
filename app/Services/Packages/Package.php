<?php

namespace App\Services\Packages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $table = 'packages';
    protected $fillable = [
        'name', 'price', 'description'
    ];

    protected $date = [
        'deleted_at'
    ];

    public function details()
    {
        return $this->hasMany(PackageDetails::class, 'package_id');
    }
}
