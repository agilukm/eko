<?php

namespace App\Services\Packages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageDetails extends Model
{
    use SoftDeletes;

    protected $table = 'package_details';
    protected $fillable = [
        'package_id', 'serve_id', 'total_unit'
    ];

    protected $date = [
        'deleted_at'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }

    public function serve()
    {
        return $this->belongsTo(\App\Services\Serves\Serve::class, 'serve_id');
    }

}
