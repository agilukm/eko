<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Instalasi

### Composer
Make sure you install composer `https://getcomposer.org/Composer-Setup.exe`

Install git `https://github.com/git-for-windows/git/releases/download/v2.18.0.windows.1/Git-2.18.0-64-bit.exe`

You can skip to step 9 if you already have project setup.

Download project from gitlab

1. Go to htdocs.
2. Right click and choose `Git Bash here`
3. type `git clone https://gitlab.com/agilukm/eko.git`
4. type `cd eko`
5. type `composer update`
6. type `php artisan key:generate`
7. On folder `eko` duplicate `.env.example` rename to `.env`
8. Setup your database and user on edit `.env` file
9. type `git pull` *to make sure everything is up to date*
10. type `php artisan migrate:fresh --seed` *becareful all data on database will be destroyed*
11. type `php artisan serve`
12. On your web browse (chrome, etc) go to url `localhost:8000`
13. Login with username: `admin@admin.com` and password: `admin`
