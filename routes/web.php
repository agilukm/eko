<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\adminController@index');

Route::group(['prefix' => 'page'], function () {

    Route::get('/', 'Front\FrontController@index');
    Route::post('/login', 'Front\FrontController@login');
    Route::post('/register', 'Front\FrontController@register');

    Route::group(['prefix' => 'user'], function() {
        Route::get('/', 'Admin\UserController@browse');
        Route::get('/', 'Admin\UserController@read');
        Route::post('/', 'Admin\UserController@add');
        Route::patch('/{id}', 'Admin\UserController@edit');
        Route::delete('/{id}', 'Admin\UserController@delete');
    });

    Route::group(['prefix' => 'jasa'], function() {
        Route::get('/tambah', function() {
            return view('Front.serves.add');
        })->name('Front_jasa_add');
        Route::get('/', 'Front\ServeController@browse')->name('Front_jasa_index');
        Route::post('/simpan', 'Front\ServeController@add')->name('Front_jasa_save');
        Route::get('/detail/{id}', 'Front\ServeController@read')->name('Front_jasa_read');
        Route::get('/{id}', 'Front\ServeController@read_ajax');
        Route::get('/{id}', 'Front\ServeController@read_ajax');
        Route::post('/edit/{id}', 'Front\ServeController@edit')->name('Front_jasa_edit');
        Route::get('/{id}/delete', 'Front\ServeController@delete')->name('Front_jasa_delete');
    });


    Route::group(['prefix' => 'paket'], function() {
        Route::get('/', 'Front\PackageController@browse');
        Route::get('/edit/{id}', 'Front\PackageController@read');
        Route::get('/{id}/details', 'Front\PackageController@details')->name('package_details');
        Route::post('/simpan', 'Front\PackageController@add');
        Route::post('/edit/{id}', 'Front\PackageController@edit');
        Route::get('/{id}/delete', 'Front\PackageController@delete');
        Route::get('/tambah', 'Front\PackageController@tambah');
    });

    Route::group(['prefix' => 'penyewaan'], function() {
        Route::get('/', 'Front\RentController@browse');
        Route::post('/simpan', 'Front\RentController@add');
        Route::get('/invoice/{id}', 'Front\RentController@invoice');
    });

    Route::group(['prefix' => 'pembayaran'], function() {
        Route::get('/', 'Front\PaymentController@browse');
        Route::get('/{id}', 'Front\PaymentController@read');
        Route::post('/{id}', 'Front\PaymentController@upload');
        Route::get('/expired/{id}', 'Front\PaymentController@expired');
    });

});

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {

    Route::get('/dashboard', 'Admin\adminController@index');
    Route::get('/email', 'Admin\adminController@email');

    Route::group(['prefix' => 'jasa'], function() {
        Route::get('/tambah', function() {
            return view('admin.serves.add');
        })->name('admin_jasa_add');
        Route::get('/', 'Admin\ServeController@browse')->name('admin_jasa_index');
        Route::post('/simpan', 'Admin\ServeController@add')->name('admin_jasa_save');
        Route::get('/edit/{id}', 'Admin\ServeController@read')->name('admin_jasa_read');
        Route::get('/{id}', 'Admin\ServeController@read_ajax');
        Route::post('/edit/{id}', 'Admin\ServeController@edit')->name('admin_jasa_edit');
        Route::get('/{id}/delete', 'Admin\ServeController@delete')->name('admin_jasa_delete');
    });

    Route::group(['prefix' => 'paket'], function() {
        Route::get('/', 'Admin\PackageController@browse');
        Route::get('/edit/{id}', 'Admin\PackageController@read');
        Route::get('/{id}/details', 'Admin\PackageController@details')->name('package_details');
        Route::post('/simpan', 'Admin\PackageController@add');
        Route::post('/edit/{id}', 'Admin\PackageController@edit');
        Route::get('/{id}/delete', 'Admin\PackageController@delete');
        Route::get('/tambah', 'Admin\PackageController@tambah');
    });

    Route::group(['prefix' => 'sewa'], function() {
        Route::get('/', 'Admin\RentController@browse');
        Route::get('/edit/{id}', 'Admin\RentController@read');
        Route::post('/simpan', 'Admin\RentController@add');
        Route::patch('/{id}', 'Admin\RentController@edit');
        Route::post('/edit/{id}', 'Admin\RentController@edit');
        Route::get('/tambah', 'Admin\RentController@tambah');
        Route::get('/{id}/delete', 'Admin\RentController@delete');
    });

    Route::group(['prefix' => 'jadwal'], function() {
        Route::get('/', 'Admin\ScheduleController@browse');
    });

    Route::group(['prefix' => 'pembayaran'], function() {
        Route::get('/', 'Admin\PaymentController@browse');
        Route::get('/edit/{id}', 'Admin\PaymentController@read');
        Route::get('/proses/{id}/{status}', 'Admin\PaymentController@process');
        Route::get('/detail_bayar/{id}', 'Admin\PaymentController@detail_bayar');
        Route::post('/simpan', 'Admin\PaymentController@add');
        Route::patch('/{id}', 'Admin\PaymentController@edit');
        Route::post('/edit/{id}', 'Admin\PaymentController@edit');
        Route::get('/invoice/{id}', 'Front\RentController@invoice');
        Route::get('/tambah', 'Admin\PaymentController@tambah');
        Route::get('/{id}/delete', 'Admin\PaymentController@delete');
    });

    Route::group(['prefix' => 'pengguna'], function() {
        Route::get('/', 'Admin\UserController@browse');
        Route::get('/edit/{id}', 'Admin\UserController@read');
        Route::get('/me', 'Admin\UserController@me');
        Route::get('/tambah', 'Admin\UserController@add');
        Route::post('/simpan', 'Admin\UserController@simpan');
        Route::post('/simpanme/{id}', 'Admin\UserController@simpanme');
        Route::post('/edit/{id}', 'Admin\UserController@edit');
        Route::get('/{id}/delete', 'Admin\UserController@delete');
    });

    Route::group(['prefix' => 'laporan'], function() {
        Route::get('/', 'Admin\ReportController@browse');
        Route::get('/export_rent', 'Admin\ReportController@export_rent');
        Route::get('/export_serve', 'Admin\ReportController@export_serve');
        Route::get('/export_package', 'Admin\ReportController@export_package');
    });

});

Auth::routes();

Route::get('/home', 'Admin\adminController@index')->name('home');
