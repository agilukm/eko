@extends('admin.layout')
@section('content')

<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Sewa</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Jadwal</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Jadwal</h4>
                        <h6 class="card-subtitle">Data Jadwal</h6>
                        <div id="calendar">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->
@endsection
@section('plugin')

<script type="text/javascript">
    $(document).ready(function() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+2; //January is 0!

        var yyyy = today.getFullYear();
        $('#calendar').fullCalendar({
            dayClick: function(date, jsEvent, view) {
                if (date.format('YYYY-MM-DD') > yyyy+'-0'+mm+'-'+dd @foreach($invalidDate as $key => $date) && date.format('YYYY-MM-DD') != '{{$date->date}}' @endforeach) {
                // This allows today and future date
                    window.open('{{url("/")}}/admin/sewa/tambah?created_at='+date.format('YYYY-MM-DD'), '_blank');
               }
            },
            header: {
                left: 'prev,next',
                center: 'title'
            },
            navLinks: true, // can click day/week names to navigate views
            editable: false,
            events: [
                @foreach($invalidDate as $key => $rent)
                    {
                        title: '{{$rent->users->name}}',
                        start: '{{$rent->date}}',
                        color: 'green',
                        textColor: 'white'
                    },
                @endforeach
            ]
        });
    });
</script>

@endsection
