@extends('admin.layout')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Jasa</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Jasa</li>
                <li class="breadcrumb-item active">Tambah</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Layanan Jasa</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">

                            <form action="{{url('admin/jasa/edit/'.$data->id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Nama</p>
                                    <input type="text" required class="form-control input-default " placeholder="Nama Jasa" name="name" value="{{$data->name}}">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Harga</p>
                                    <input type="number" required class="form-control input-default " placeholder="Harga" name="price" value="{{$data->price}}">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Satuan</p>
                                    <input type="text" class="form-control input-default " placeholder="Satuan" name="unit" required value="{{$data->unit}}">
                                </div>
                                <center><button type="submit" class="btn btn-success" name="button">Simpan</button></center>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /# row -->

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

@endsection
