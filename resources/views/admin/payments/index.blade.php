@extends('admin.layout')
@section('content')

<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Paket</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Pembayaran</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Pembayaran</h4>
                        <h6 class="card-subtitle">Data Pembayaran</h6>
                        <div class="table-responsive m-t-40">
                            <a href="{{url('/admin')}}/pembayaran/tambah"><button type="button" class="btn btn-success pull-right" name="button">Tambah</button></a>
                            <table id="myTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width='1%'>No.</th>
                                        <th>Name</th>
                                        <th>File</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $key => $value): ?>
                                        <tr class="{{$key}}">
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->rents->users->name}}</td>
                                            <td><img src="{{$value->file}}" alt="" width="75" height="75"></td>
                                            <td>{{$value->total}}</td>
                                            <td>{{$value->statusLabel()}}</td>
                                            <td>
                                                <div class="dropdown">
                                                @if($value->file)
                                                <a href="{{$value->file}}" target="_blank"><button type="button" name="button" class="btn btn-info">Bukti</button></a>
                                                @endif
                                                @if($value->status == 0)
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Pilih
                                                    <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                      <li><a href="{{url('/admin')}}/pembayaran/proses/{{$value->id}}/1">Terima</a></li>
                                                      <li><a href="{{url('/admin')}}/pembayaran/proses/{{$value->id}}/2">Tolak</a></li>
                                                    </ul>
                                                 @endif
                                             </div>
                                                <!-- <a href="pembayaran/edit/{{$value->id}}"><button type="button" class="btn btn-warning">Edit</button></a> -->
                                                <!-- <button type="button" name="hapus" id="{{$key}}" class="conf btn btn-danger" value="pembayaran/{{$value->id}}/delete">Hapus</button> -->
                                            </td>
                                        </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
@endsection
