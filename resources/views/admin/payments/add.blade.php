@extends('admin.layout')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pembayaran</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Pembayaran</li>
                <li class="breadcrumb-item active">Tambah</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Pembayaran Sewa</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">

                            <form action="{{url('admin/pembayaran/simpan')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Penyewa</p>
                                    <select class="form-control" name="rent_id" required id="rent_id">
                                        <option value="">Pilih</option>
                                        @foreach ($delay as $key => $value)
                                            <option value="{{$value->id}}">{{$value->users->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Total</p>
                                    <input type="number" required class="form-control input-default" readonly placeholder="Total" id="total" name="final">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Dibayar</p>
                                    <input type="number" class="form-control input-default " placeholder="Dibayar" readonly name="paid" required id="paid">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Sisa</p>
                                    <input type="number" class="form-control input-default " placeholder="Sisa" readonly name="left" required id="sisa">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Jumlah Bayar</p>
                                    <input type="text" readonly class="form-control input-default " placeholder="Jumlah Bayar" name="final" required id="final">
                                </div>
                                <center><button type="submit" class="btn btn-success" name="button">Simpan</button></center>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /# row -->

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

@endsection

@section('plugin')
<script type="text/javascript">
$("#rent_id").change( function() {
    $.ajax({
        url: "{{ url('admin/pembayaran/detail_bayar') }}/" + $(this).val(),
        type: "GET",
        dataType: "html",
        success: function (result) {
            var datas = JSON.parse(result);
            $('#total').val(datas.total);
            $('#paid').val(datas.paid);
            $('#sisa').val(datas.rest);
            $('#final').val(datas.rest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
                swal("Gagal!", "Silahkan coba lagi", "error");
        }
    });
});
</script>
@endsection
