@extends('admin.layout')
@section('content')

<form action="{{url('admin/sewa/simpan')}}" method="post">
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Sewa</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Sewa</li>
                <li class="breadcrumb-item active">Tambah</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Sewa</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Pelanggan</p>
                                    <select class="form-control select2" required name="user_id" required>
                                        <option value="">Pilih</option>
                                        @foreach($users as $key => $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Tanggal</p>
                                    <input type="text" id="daterange" required class="form-control input-default " name="date"
                                            @if(isset($_GET['created_at']))
                                                value="{{$_GET['created_at']}}" readonly
                                            @endif>
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Tipe</p>
                                    <select class="form-control" required name="type" id="tipe" required>
                                        <option value="">Pilih</option>
                                        <option value="Paket">Paket</option>
                                        <option value="Reguler">Reguler</option>
                                    </select>
                                </div>
                                <div id="list">

                                </div>


                                <center><h2>List Jasa</h2></center>
                                <br>

                                <!--  duplicate field -->
                                <div class="input-group control-group after-add-more">
                                </div>

                                <!--  duplicate field -->
                                <div class="input-group total-reguler">
                                </div>

                                <center><button type="submit" class="btn btn-success" name="button">Simpan</button></center>

                        </div>
                    </div>
                    <!-- end card -->
                </div>
            </div>
        </div>
        <!-- /# row -->
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

</form>
@endsection
@section('plugin')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
function getFormatDate(d){
    return d.getMonth()+1 + '/' + d.getDate() + '/' + d.getFullYear()
}

$(document).ready(function() {
    mdTemp = new Date(),
    maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 30)));

    var invalidDate = [
        @foreach($invalidDate as $key => $date)
        '{{$date->date}}',
        @endforeach
    ];

@if(!isset($_GET['created_at']))
    $('#daterange').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: maxDate,
        isInvalidDate: function(date) {
            for (var ii = 0; ii < invalidDate.length; ii++){
                if (date.format('YYYY-MM-DD') == invalidDate[ii]){
                  return true;
                }
              }
        }
    },
    function(chosen_date) {
      $('#daterange').val(chosen_date.format('YYYY-MM-DD'));
    });
@endif
});

function hitung(id) {
    var qty = $('#qty'+id).val();
    var total_jasa = qty*$('#harga'+id).val();
    $('#total'+id).val(total_jasa);
    calculateSum();
}

function calculateSum() {
	var sum = 0;
	//iterate through each textboxes and add the values
	$(".total").each(function() {

		//add only if the value is number
		if(!isNaN(this.value) && this.value.length!=0) {
			sum += parseFloat(this.value);
		}

	});
	//.toFixed() method will roundoff the final sum to 2 decimal places
	$("#totalsemua").val(sum);
}

$("#tipe").change( function() {
    $(".total-reguler").html('');
    $('#list').html('');
    $('#harga').remove('');
    if ($(this).val() == 'Paket') {
        $('#list').html('');
        $('.duplicate').remove();
        $('#list').append('<div class="form-group" id="package"> <p class="text-muted m-b-15 f-s-12">Paket</p> <select id="select_package" class="form-control package" name="package" required style="height:42px" required> <option value="">Pilih</option> @foreach($packages as $package) <option value="{{$package->id}}">{{$package->name}}</option> @endforeach </select> </div>');
        $('.select2').select2();
    }
    else if ($(this).val() == 'Reguler') {
        $(".total-reguler").html('</div><div class="form-group col-md-12"> <p class="text-muted m-b-15 f-s-12">Total</p> <input type="text" required class="form-control input-default " placeholder="Total" name="final" id="totalsemua" readonly> </div>');
        $('#list').html('');
        $('.duplicate').remove();
        $('#list').append('<div class="form-group" id="serve"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <select id="select_serve" class="form-control serve select2" multiple name="serve[]" required style="height:42px" required> @foreach($serves as $serve) <option value="{{$serve->id}}">{{$serve->name}}</option> @endforeach </select> </div>');
        $('.select2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });

        $(".serve").on("select2:select", function (e) {
            var lastSelectedId = e.params.data.id;
            var lastSelectedName = e.params.data.text;
            $.ajax({
                url: "{{ url('admin/jasa/') }}/" + lastSelectedId,
                type: "GET",
                dataType: "html",
                success: function (result) {
                    var datas = JSON.parse(result);
                    var duplicate = '<div class="duplicate form-group col-md-3 nama_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Jasa</p> <input type="text" name="" value="'+datas.name+'" class="form-control" disabled><input type="hidden" name="" value="'+datas.id+'" class="form-control"> </div> <div class="duplicate form-group col-md-3 harga_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Harga</p> <input type="number" name="harga[]" class="form-control" id="harga'+datas.id+'" placeholder="Harga" required readonly value="'+datas.price+'"></div><div class="duplicate form-group col-md-3 jumlah_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" id="qty'+datas.id+'" name="qty[]" class="form-control qty" min="1" placeholder="Jumlah Dipesan" required onkeyup="hitung('+datas.id+')"></div> <div class="duplicate form-group col-md-3 total_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Total Harga</p> <input type="text" readonly name="total[]" class="form-control total" placeholder="Total Harga" required value="" readonly id="total'+datas.id+'"></div>';
                    $(".after-add-more").append(duplicate);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Silahkan coba lagi", "error");
                }
            });
        });

        $(".serve").on("select2:unselect", function (e) {
            var lastSelectedId = e.params.data.id;
            var lastSelectedName = e.params.data.text;
            $('.nama_jasa'+lastSelectedId).remove();
            $('.harga_jasa'+lastSelectedId).remove();
            $('.jumlah_jasa'+lastSelectedId).remove();
            $('.total_jasa'+lastSelectedId).remove();
            calculateSum();
        });

    }
    else {
        $('#list').html('');
        $('.duplicate').remove();
    }

    $("#select_package").change( function() {
        $('#harga').remove('');
        $('.after-add-more').html('');
        $.ajax({
            url: "{{ url('admin/paket/') }}/" + $(this).val() + "/details",
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                $("#list").append(' <div class="form-group" id="harga"> <p class="text-muted m-b-15 f-s-12">Harga</p> <input type="text" required class="form-control input-default " name="" readonly value="'+format_number(datas.data.price, "Rp")+'"> <input type="hidden" required class="form-control input-default " name="final" readonly value="'+datas.data.price+'" id="harga-final"> </div>');

                $.each(datas.data.details, function(k,v){
                    $('.after-add-more').append('<div class="duplicate form-group col-md-6 nama_jasa" id=""><p class="text-muted m-b-15 f-s-12">Jasa</p><input type="hidden" name="serve[]" value="'+datas.data.details[k].serve.id+'"><input type="text" name="" value="'+datas.data.details[k].serve.name+'" class="form-control" disabled> </div> <div class="duplicate form-group col-md-6 jumlah_jasa" id=""> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" name="qty[]" class="form-control" value="'+datas.data.details[k].total_unit+'" placeholder="Jumlah" required readonly><input type="hidden" value="0" name="total[]"></div> </div>');
                });
                $('#list').append('<div class="form-group" id="pembayaran"> <p class="text-muted m-b-15 f-s-12">Pembayaran</p> <select class="form-control package" name="payment_type" required id="payment_type"> <option value="">Pilih</option> <option value="Dp">Dp</option><option value="Tunai">Tunai</option>  </select> </div></div><div class="after-payment"></div>');
                $('#payment_type').change( function() {
                    if ($(this).val() == 'Dp') {
                        var harga_final = $('#harga-final').val();
                        var dp = harga_final*0.5;
                        $('.after-payment').html('<div class="form-group id="value-payment"><p class="text-muted m-b-15 f-s-12">Uang Muka</p><input type="number" class="form-control" required name="dp" min="'+dp+'" max="'+harga_final+'"></div>');
                    } else {
                        $('.after-payment').html('');
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
    });

});
</script>
@endsection
