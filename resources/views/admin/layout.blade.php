<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="{{ url('/')}}/admin/images/admin/image/png" sizes="16x16" href="{{ url('/')}}/admin/images/favicon.png">
    <title>Ela - Bootstrap Admin Dashboard Template</title>
    <!-- Bootstrap Core CSS -->

        <link rel="stylesheet" type="text/css" href="{{ url('/')}}/admin/css/bootstrap-datetimepicker.min.css" />
    <link href="{{ url('/')}}/admin/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="{{ url('/')}}/admin/css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="{{ url('/')}}/admin/css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="{{ url('/')}}/admin/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="{{ url('/')}}/admin/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="{{ url('/')}}/admin/css/helper.css" rel="stylesheet">
    <link href="{{ url('/')}}/admin/css/style.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{url('/')}}/admin/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">
    <link rel="stylesheet" media="print" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css">
    <link rel="stylesheet" href="{{ url('/') }}/admin/css/lib/html5-editor/bootstrap-wysihtml5.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b>Cherariz</b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>Wedding</span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">

                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Comment -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
								<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Pembayaran</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            @php
                                                $payments = \App\Services\Payments\Payment::where('status',0)->get();
                                            @endphp
                                            <!-- Message -->
                                            @foreach($payments as $payment)
                                            <a href="{{url ('/admin/pembayaran') }}">
                                                <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-money"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>{{$payment->rents->users->name}}</h5> <span class="mail-desc">{{number_format($payment->total)}}</span> <span class="time">{{$payment->updated_at}}</span>
                                                </div>
                                            </a>
                                            @endforeach
                                            <!-- Message -->
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Comment -->
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ url('/')}}/admin/images/users/5.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="{{url('/admin/pengguna/me')}}"><i class="ti-user"></i> Profile</a></li>
                                    <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-power-off"></i> Logout</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a href="{{ url('/admin/') }}/dashboard"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</a></li>
                        <li> <a href="{{ url('/admin/') }}/jasa"><i class="fa fa-stack-exchange"></i><span class="hide-menu">Jasa</a></li>
                        <li> <a href="{{ url('/admin/') }}/paket"><i class="fa fa-stack-exchange"></i><span class="hide-menu">Paket</a></li>
                        <li> <a href="{{ url('/admin/') }}/sewa"><i class="fas fa-handshake"></i><span class="hide-menu">Penyewaan</a></li>
                        <li> <a href="{{ url('/admin/') }}/jadwal"><i class="fas fa-calendar"></i><span class="hide-menu">Jadwal</a></li>
                        <li> <a href="{{ url('/admin/') }}/pembayaran"><i class="fas fa-money-bill-alt"></i><span class="hide-menu">Pembayaran</a></li>
                        <li> <a href="{{ url('/admin/') }}/pengguna"><i class="fa fa-group"></i><span class="hide-menu">Pengguna</a></li>
                        <li> <a href="{{ url('/admin/') }}/laporan"><i class="fa fa-book"></i><span class="hide-menu">Laporan</a></li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->



    <div class="modal fade" id="errormodal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Error</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">


                  @if($errors->any())
                      @foreach ($errors->all() as $error)
                          <div>{{ $error }}</div>
                      @endforeach
                  @endif

              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>

          </div>
        </div>
    </div>
    @yield('content')

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ url('/')}}/admin/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ url('/')}}/admin/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.4/metisMenu.min.js" integrity="sha256-YqTSp9JlAIbtktRVVwWzOEXJAIs8vE/r79vR7jcvYj0=" crossorigin="anonymous"></script>
    <script src="{{ url('/')}}/admin/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ url('/')}}/admin/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <!--stickey kit -->
    <script src="{{ url('/')}}/admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

    <!--Custom JavaScript -->
    <script type="text/javascript" src="{{ url('/')}}/admin/js/moment.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>

    <script src="{{ url('/')}}/admin/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{ url('/')}}/admin/js/custom.min.js"></script>

    <script src="{{ url('/') }}/admin/js/lib/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="{{ url('/') }}/admin/js/lib/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="{{ url('/') }}/admin/js/lib/html5-editor/wysihtml5-init.js"></script>
    @yield('plugin')
    <script type="text/javascript">
    function format_number(n, currency) {
        return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }

    $(document).ready( function () {



        var table = $('#myTable').DataTable();
        $('.select2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });
        // $('#myTable tbody').on( 'click', 'tr', function () {
        //     if ( $(this).hasClass('selected') ) {
        //         $(this).removeClass('selected');
        //     }
        //     else {
        //         table.$('tr.selected').removeClass('selected');
        //         $(this).addClass('selected');
        //     }
        // });

        $('.conf').click( function() {
            var conf = confirm(this.value, this.id, this.name);
        });

        function confirm(uri, id, name){
            swal({
                title: "Data akan di"+name,
                text: "Apakah anda yakin?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yakin",
                cancelButtonText: "Batal",
                confirmButtonColor: "#ec6c62"
            }, function (isConfirm) {
                if (!isConfirm) return;
                table.row($('.'+id)).remove().draw( false );
                    $.ajax({
                        url: uri,
                        type: "GET",
                        dataType: "html",
                        success: function () {
                            var asd = table.row($('.'+id)).remove().draw( false );
                            if(asd)
                            {
                                swal("Behasil!", "Data berhasil di"+name, "success");
                            }
                        },

                        error: function (xhr, ajaxOptions, thrownError) {
                                swal("Behasil!", "Data berhasil di"+name, "success");
                        }
                    });
            });
        }
    });
    </script>

    @if(session()->has('message'))
    <script type="text/javascript">
        swal("Behasil!", "{{ session()->get('message') }}", "success", 3000, false);
    </script>
    @endif

    @if($errors->any())
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#errormodal').modal('show');
        });
    </script>
    @endif

</body>
</html>
