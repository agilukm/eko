@extends('admin.layout')
@section('content')

<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Paket</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Paket</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Paket</h4>
                        <h6 class="card-subtitle">Data Paket</h6>
                        <div class="table-responsive m-t-40">
                            <a href="{{url('/admin')}}/paket/tambah"><button type="button" class="btn btn-success pull-right" name="button">Tambah</button></a>
                            <table id="myTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width='1%'>No.</th>
                                        <th>Name</th>
                                        <th>Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $key => $value): ?>
                                        <tr class="{{$key}}">
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->price}}</td>
                                            <td>
                                                <a href="#" class="btn btn-info" data-toggle="modal" data-target="#detailModal{{$value->id}}">Detail</a>
                                                <a href="{{url('/admin/')}}/paket/edit/{{$value->id}}"><button type="button" class="btn btn-warning">Edit</button></a>
                                                <button type="button" name="hapus" id="{{$key}}" class="conf btn btn-danger" value="{{url('/admin/')}}/paket/{{$value->id}}/delete">Hapus</button>
                                            </td>
                                        </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- End footer -->
</div>
<?php foreach ($data as $key => $value): ?>
    <!-- Modal -->
    <div class="modal fade" id="detailModal{{$value->id}}">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Paket {{$value->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-hover">
                    <thead>
                        <th>No</th>
                        <th>Nama Jasa</th>
                        <th>Jumlah</th>
                        <th>Satuan</th>
                    </thead>
                    <tbody>
                        <?php foreach ($value->details as $key => $details): ?>
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$details->serve->name}}</td>
                            <td>{{$details->total_unit}}</td>
                            <td>{{$details->serve->unit}}</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>

                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
      </div>
    </div>
    <!--  End Modal -->
<?php endforeach; ?>
<!-- End Page wrapper  -->
@endsection
@section('plugin')
<script type="text/javascript">



</script>
@endsection
