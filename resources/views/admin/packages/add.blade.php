@extends('admin.layout')
@section('content')

<form action="{{url('admin/paket/simpan')}}" method="post">
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Paket</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Paket</li>
                <li class="breadcrumb-item active">Tambah</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Paket Jasa</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Nama</p>
                                    <input type="text" required class="form-control input-default " placeholder="Nama Paket" name="name">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Harga</p>
                                    <input type="number" required class="form-control input-default " placeholder="Harga Paket" name="price">
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Deskripsi</p>
                                    <textarea  name="description" class="textarea_editor form-control" rows="15" placeholder="Enter text ..." style="height:450px"></textarea>
                                </div>
                                <div class="form-group">
                                    <p class="text-muted m-b-15 f-s-12">Jasa</p>
                                    <select class="form-control serve select2" multiple name="serve[]" required style="height:42px" required>
                                        @foreach($serves as $serve)
                                            <option value="{{$serve->id}}">{{$serve->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <center><h2>List Jasa</h2></center>
                                <br>

                                <!--  duplicate field -->
                                <div class="input-group control-group after-add-more">
                                </div>

                                <center><button type="submit" class="btn btn-success" name="button">Simpan</button></center>

                        </div>
                    </div>
                    <!-- end card -->
                </div>
            </div>
        </div>
        <!-- /# row -->
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

</form>
@endsection
@section('plugin')
<script type="text/javascript">

$(".serve").on("select2:select", function (e) {
    var lastSelectedId = e.params.data.id;
    var lastSelectedName = e.params.data.text;
    var duplicate = '<div class="form-group col-md-5" id="nama_jasa'+lastSelectedId+'"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <input type="text" name="" value="'+lastSelectedName+'" class="form-control" disabled> </div> <div class="form-group col-md-5" id="jumlah_jasa'+lastSelectedId+'"> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" name="jumlah[]" class="form-control" placeholder="Jumlah" required></div></div>';
    $(".after-add-more").append(duplicate);
});

$(".serve").on("select2:unselect", function (e) {
    var lastSelectedId = e.params.data.id;
    var lastSelectedName = e.params.data.text;
    $('#nama_jasa'+lastSelectedId).remove();
    $('#jumlah_jasa'+lastSelectedId).remove();
});

</script>
@endsection
