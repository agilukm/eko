<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

            <table border="1">
                <thead>
                    <tr>
                        <td colspan="5" align="center"><center> <b>LAPORAN DATA PAKET CHERARIZ WEDDING</b></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">Nama</th>
                        <th align="center">Harga</th>
                        <th align="center">Jasa</th>
                        <th align="center">Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($packages as $key => $package)
                        <tr>
                            <td  align="center" valign="middle">{{$key+1}}</td>
                            <td  align="center" valign="middle">{{$package->name}}</td>
                            <td  align="center" valign="middle">{{number_format($package->price)}}</td>
                            <td  align="center" valign="middle">
                                @foreach($package->details as $detail)
                                    {{$detail->serve->name}} <br>
                                @endforeach
                            </td>
                            <td  align="center" valign="middle">
                                @foreach($package->details as $detail)
                                    {{$detail->total_unit}} <br>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
    </body>
</html>
