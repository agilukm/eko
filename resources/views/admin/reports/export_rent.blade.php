<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

            <table border="1">
                <thead>
                    <tr>
                        <td    colspan="9" align="center"><center> <b>LAPORAN PENYEWAAN CHERARIZ WEDDING</b></td>
                    </tr>
                    <tr>
                        <td   colspan="9" align="center"><center> Tanggal Periode : {{$dari}} s/d {{$sampai}}</td>
                    </tr>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">Tgl</th>
                        <th align="center">Nama Penyewa</th>
                        <th align="center">Nama Jasa</th>
                        <th align="center">Alamat Acara</th>
                        <th align="center">Tipe Penyewaan</th>
                        <th align="center">Jumlah</th>
                        <th align="center">Harga</th>
                        <th align="center">Diskon</th>
                        <th align="center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rent as $key => $ren)
                        <tr>
                            <td  align="center" valign="middle">{{$key+1}}</td>
                            <td  align="center" valign="middle">{{$ren->date}}</td>
                            <td  align="center" valign="middle">{{$ren->users->name}}</td>
                            <td  align="center" valign="middle">
                                @foreach($ren->details as $key => $de)
                                        {{$de->serves->name}} <br> <br>
                                @endforeach
                            </td>
                            <td  align="center" valign="middle">{{$ren->address}}</td>
                            <td  align="center" valign="middle">{{$ren->type}}</td>
                            <td  align="center" valign="middle">
                                @foreach($ren->details as $key => $de)
                                    {{$de->qty}} <br> <br>
                                @endforeach
                            </td>
                            <td  align="center" valign="middle">
                                @if($ren->type == 'Paket')
                                    @foreach($ren->details as $key => $de)
                                        0 <br> <br>
                                    @endforeach
                                @else
                                    @foreach($ren->details as $key => $de)
                                        {{number_format($de->serves->price)}} <br> <br>
                                    @endforeach
                                @endif
                            </td>
                            <td  align="center" valign="middle">{{number_format($ren->discount)}}</td>
                            <td  align="center" valign="middle">
                                    {{number_format($ren->total-$ren->discount)}}
                            </td>
                        </tr>
                    @endforeach
                        <tr>
                            <td colspan="8"></td>
                            <td  align="right"><b> Total :</td>
                            <td  align="center">{{number_format($total-$discount)}}</td>
                        </tr>
                </tbody>

            </table>
    </body>
</html>
