<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

            <table border="1">
                <thead>
                    <tr>
                        <td colspan="6" align="center"><center> <b>LAPORAN DATA JASA CHERARIZ WEDDING</b></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">Nama</th>
                        <th align="center">Harga</th>
                        <th align="center">Unit</th>
                        <th align="center">Minimal Penyewaan</th>
                        <th align="center">Stok</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($serves as $key => $serve)
                        <tr>
                            <td  align="center" valign="middle">{{$key+1}}</td>
                            <td  align="center" valign="middle">{{$serve->name}}</td>
                            <td  align="center" valign="middle">{{number_format($serve->price)}}</td>
                            <td  align="center" valign="middle">Per {{$serve->unit}}</td>
                            <td  align="center" valign="middle">{{$serve->min}}</td>
                            <td  align="center" valign="middle">{{$serve->stock}}</td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
    </body>
</html>
