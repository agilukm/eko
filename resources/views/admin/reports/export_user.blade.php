<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

            <table border="1">
                <thead>
                    <tr>
                        <td colspan="6" align="center"><center> <b>LAPORAN DATA PAKET CHERARIZ WEDDING</b></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">Nama</th>
                        <th align="center">Alamat</th>
                        <th align="center">Email</th>
                        <th align="center">No. Hp</th>
                        <th align="center">Hak Akses</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        <tr>
                            <td  align="center" valign="middle">{{$key+1}}</td>
                            <td  align="center" valign="middle">{{$user->name}}</td>
                            <td  align="center" valign="middle">{{$user->address}}</td>
                            <td  align="center" valign="middle">{{$user->email}}</td>
                            <td  align="center" valign="middle">{{$user->phone}}</td>
                            <td  align="center" valign="middle">
                                @if($user->roles == 1 && $user->owner == 1)
                                    Pemilik
                                @elseif($user->roles == 1 && $user->owner == 0)
                                    Admin
                                @else
                                    Pelanggan
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
    </body>
</html>
