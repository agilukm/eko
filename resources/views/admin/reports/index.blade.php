@extends('admin.layout')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Laporan</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Laporan</li>
                <li class="breadcrumb-item active">Cetak Laporan</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <center><h4>Penyewaan</h4><center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('admin/laporan/export_rent')}}" method="get" target="_blank">
                                    <div class="form-group">
                                        <p class="text-muted m-b-15 f-s-12">Dari Tanggal</p>
                                        <input type="date" required class="form-control input-default " placeholder="Dari Tanggal" name="dari">
                                    </div>
                                    <div class="form-group">
                                        <p class="text-muted m-b-15 f-s-12">Sampai Tanggal</p>
                                        <input type="date" required class="form-control input-default " placeholder="Sampai Tanggal" name="sampai">
                                    </div>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Jasa</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('admin/laporan/export_rent')}}" method="get" target="_blank">
                                    <div class="form-group">
                                        <br>
                                        <br>
                                        <center><p>Cetak Laporan Jasa</p></center>
                                        <br>
                                        <br>
                                    </div>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Paket</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('admin/laporan/export_package')}}" method="get" target="_blank">
                                    <div class="form-group">
                                        <br>
                                        <br>
                                        <center><p>Cetak Laporan Paket</p></center>
                                        <br>
                                        <br>
                                    </div>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Pengguna</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('admin/laporan/export_user')}}" method="get" target="_blank">
                                    <div class="form-group">
                                        <br>
                                        <br>
                                        <center><p>Cetak Laporan Pengguna</p></center>
                                        <br>
                                        <br>
                                    </div>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
        </div>
    </div>
@endsection
