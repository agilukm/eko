
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!-- Include the above in your HEAD tag -->
        <style type="text/css" media="print">
        @media print {
  @page { margin: 0; overflow: hidden !important;}
  body { margin: 1.6cm; overflow-x: hidden !important; overflow-y: hidden !important;}
  table {overflow: hidden !important; }
}
        </style>
    </head>
    <body>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    		     <div class="col-md-12">
    			<center><h2>Wedding</h2></center>
    			<strong class="pull-right">Kode Pemesanan: {{$data->code}}</strong>
    		</div>
    		</div>
            <br>
            <br>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Pemesan:</strong><br>
    					{{$data->users->name}}<br>
    				</address>
    			</div>
                <div class="col-xs-6 text-right">
    				<address>
    					<strong>Tipe Pemesanan</strong><br>
    					{{$data->type}}
    				</address>
    			</div>

    		</div>
    		<div class="row">
                <div class="col-xs-6">
                    <address>
    					<strong>Tanggal Pemesanan:</strong><br>
    					{{$data->created_at}}<br><br>
    				</address>
                </div>
    			<div class="col-xs-6 text-right">
                    <address>
                        <strong>Tanggal Pelaksanaan:</strong><br>
                        {{$data->date}}<br><br>
                    </address>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Detail Pemesanan</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="">
                        @if($data->type == 'Paket')
    					<table class="table" width="100%" style="overflow: hidden;">
    						<thead>
                                <tr>
        							<td class="text-center"><strong>Jasa</strong></td>
        							<td class="text-center"><strong>Jumlah</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							@foreach ($data->details as $details)
    							<tr>
                                    <td class="text-center">{{$details->serves->name}}</td>
    								<td class="text-center">{{$details->qty}}</td>
    							</tr>
                                @php
                                    $subtotal[] = $details->total
                                @endphp
                                @endforeach
                                <tr>

                                </tr>
    							<tr>
    								<td class="thick-line text-right"><strong>Sub Total Harga</strong></td>
    								<td class="thick-line text-right">{{'Rp. '.number_format($data->total)}}</td>
    							</tr>
    							<tr>
    								<td class="no-line text-right"><strong>Diskon</strong></td>
    								<td class="no-line text-right">{{'Rp. '.number_format($data->discount)}}</td>
    							</tr>
    							<tr>
    								<td class="no-line text-right"><strong>Total</strong></td>
    								<td class="no-line text-right">{{'Rp. '.number_format($data->total-$data->discount)}}</td>
    							</tr>
    						</tbody>
    					</table>
                        @endif
                        @if($data->type != 'Paket')
    					<table class="table" width="100%" style="overflow: hidden;">
    						<thead>
                                <tr>
        							<td class="text-center"><strong>Jasa</strong></td>
        							<td class="text-center"><strong>Jumlah</strong></td>
        							<td class="text-center"><strong>Harga</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							@foreach ($data->details as $details)
    							<tr>
                                    <td class="text-center">{{$details->serves->name}}</td>
    								<td class="text-center">{{$details->qty}}</td>
    								<td class="text-center">{{'Rp. '.number_format($details->serves->price)}}</td>
    								<td class="text-right">{{'Rp. '.number_format($details->total)}}</td>
    							</tr>
                                @php
                                    $subtotal[] = $details->total
                                @endphp
                                @endforeach
                                <tr>

                                </tr>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Sub Total</strong></td>
    								<td class="thick-line text-right">{{'Rp. '.number_format($data->total)}}</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Diskon</strong></td>
    								<td class="no-line text-right">{{'Rp. '.number_format($data->discount)}}</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">{{'Rp. '.number_format($data->total-$data->discount)}}</td>
    							</tr>
    						</tbody>
    					</table>
                        @endif
    				</div>
    			</div>
    		</div>
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Detail Pembayaran</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="">
    					<table class="table" width="100%" style="overflow: hidden;">
    						<thead>
                                <tr>
        							<td><strong>Keterangan</strong></td>
        							<td class="text-center"><strong>Tanggal Pembayaran</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							@foreach ($data->payments as $payment)
    							<tr>
    								<td>{{$payment->type}}</td>
    								<td class="text-center">{{$payment->created_at}}</td>
    								<td class="text-right">{{'Rp. '.number_format($payment->total)}}</td>
    							</tr>
                                @endforeach

    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">{{'Rp. '.number_format($data->total-$data->discount)}}</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<script src="{{ url('/')}}/admin/js/lib/jquery/jquery.min.js"></script>

<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
    window.print();
    window.close();
</script>

    </body>
</html>
