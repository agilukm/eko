@extends('front.layout')
@section('content')
<!-- Content -->
<!-- Content -->
<div class="content clearfix">

    <div class="section-block replicable-content">
        <!-- Product Details -->
        <div class="row product">
            <div class="column width-6">
                <div class="product-images">
                    <div class="thumbnail product-thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">
                        @if($data->type == 'file')
                            <a class="overlay-link lightbox-link" data-group="product-lightbox-gallery" href="{{$data->fileurl}}">
                            <img src="{{$data->fileurl}}" alt="" style="width:200px; height:400px"/>
                        @endif

                        @if($data->type == 'url')
                            <a class="overlay-link lightbox-link" data-group="product-lightbox-gallery" href="{{$data->fileurl}}">
                            <img src="{{$data->fileurl}}" alt="" style="width:200px; height:400px"/>
                        @endif
                            <span class="overlay-info">
                                <span>
                                    <span>
                                        Enlarge
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column width-5 offset-1">
                <div class="product-summary">
                    <h1 class="product-title">{{$data->name}}</h1>
                    <div class="product-price price"><ins><span class="amount">Rp {{number_format($data->price)}} / {{$data->unit}}</span></ins></div>
                    <!-- <div class="product-description">
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                    </div> -->
                    <div class="product-description">
                        <div class="accordion product-addtional-info style-2" data-toggle-icon>
                            <ul>
                                <li>
                                    <a href="#accordion-more-info">Deskripsi</a>
                                    <div id="accordion-more-info">
                                        <div class="accordion-content">
                                            <p>{!! nl2br($data->description) !!}</<p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <hr class="hide show-on-mobile">
                </div>
            </div>
        </div>
        <!-- Product Details End -->


    </div>
</div>
<!-- Content End -->
@endsection
@section('plugin')
<script type="text/javascript">

var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";

</script>
@endsection
