@extends('front.layout')
@section('content')
<!-- Content -->
<!-- Content -->
<div class="content clearfix">
    <br><br>
    <div class="section-block pt-80 no-padding-bottom">
        <div class="row">
            <div class="class width-12">
                <h2 align="center">Paket</h2>
            </div>
            <div class="column width-12">
                <hr>
            </div>
        </div>
    </div>
    @foreach($data as $key => $package)
    <!-- Tabs Style 1 -->
    <div class="section-block pt-60 pb-0">
        <div class="row">
            <div class="column width-4">
                <h3 class="mb-50">{{$package->name}}</h3>
            </div>
            <div class="column width-8">
                <div class="tabs style-1 left">
                    <ul class="tab-nav">
                        <li class="active">
                            <a href="#tabs-{{$package->id}}-pane-0">Deskripsi</a>
                        </li>
                        <li class="">
                            <a href="#tabs-{{$package->id}}-pane-1">Harga</a>
                        </li>
                        <li>
                            <a href="#tabs-{{$package->id}}-pane-2">Detail Barang</a>
                        </li>
                    </ul>
                    <div class="tab-panes">
                        <div id="tabs-{{$package->id}}-pane-0" class="active animate">
                            <div class="tab-content">
                                {!! nl2br($package->description) !!}
                            </div>
                        </div>
                        <div id="tabs-{{$package->id}}-pane-1" class="">
                            <div class="tab-content">
                                <p class="lead">Rp {{number_format($package->price)}}</p>
                            </div>
                        </div>
                        <div id="tabs-{{$package->id}}-pane-2">
                            <div class="tab-content">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Barang</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($package->details as $key2 => $serve)
                                            <tr>
                                                <td>{{++$key2}}</td>
                                                <td>{{$serve->serve->name}}</td>
                                                <td>{{$serve->total_unit}} / {{$serve->serve->unit}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Tabs Style 1 End -->
    @endforeach


</div>
<!-- Content End -->
@endsection
@section('plugin')
<script type="text/javascript">

var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";

</script>
@endsection
