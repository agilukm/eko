@extends('front.layout')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

@section('content')
<!-- Content -->
<div class="content clearfix">
<br><br>
    <div class="section-block pt-80 no-padding-bottom">
        <div class="row">
            <div class="class width-12">
                <h2 align="center">Jasa</h2>
            </div>
            <div class="column width-12">
                <hr>
            </div>
        </div>
    </div>

    <!-- Product Grid -->
    <div id="product-grid" class="section-block grid-container products fade-in-progressively full-width no-padding-top" data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-resize data-animate-resize-duration="0">
        <div class="row">
            <div class="column width-12">
                <div class="row grid content-grid-5">
                    @foreach($data as $key => $jasa)
                    <div class="grid-item product portrait grid-sizer design"  onclick="select_item({{$jasa->id}})">
                        <div class="" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">

                                @if($jasa->type == 'file')
                                <img src="{{asset($jasa->fileurl)}}" alt="" style="width:200px; height:400px"/>
                                @endif
                                @if($jasa->type == 'url')
                                <img src="{{$jasa->fileurl}}" alt="" style="width:200px; height:400px"/>
                                @endif

                        </div>
                        <div class="product-details center">
                            <h3 class="product-title">
                                <a href="{{ URL('/page/jasa/detail/'.$jasa->id) }}">
                                    {{$jasa->name}}
                                </a>
                            </h3>
                            <span class="product-price price"><ins><span class="amount">Rp {{number_format($jasa->price)}} / {{$jasa->unit}}</span></ins></span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @if(Auth::check())
    <form action="{{url('page/penyewaan/simpan')}}" method="post">
    <div class="section-block pt-60 pb-0">
        <div class="row">
            <div class="column width-12">
                <div id="pemesanan_">
                    <div class="card-body">
                    <div class="basic-form" id="hahahahaha">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-fname form-element large" required name="user_id" required value="{{Auth::user()->id}}">
                            <input type="hidden" class="form-fname form-element large" required name="type" required value="Reguler">

                            <div class="form-group">
                                <p class="text-muted m-b-15 f-s-12">Alamat Penyewaan</p>
                                <input type="text" class="form-fname form-element large" required name="address" required>
                            </div>
                            <div class="" id="datetimehe">
                                <div class="form-group"> <p class="text-muted m-b-15 f-s-12">Tanggal</p> <input type="text" id="daterange" autocomplete="off" required class="form-fname form-element large " name="date"> </div> <div class="form-group"> <p class="text-muted m-b-15 f-s-12">Jam</p> <input type="text" id="timepicker" autocomplete="off" required class="form-fname form-element large " name="time"> </div>
                            </div>
                            <div id="list">

                            </div>


                            <center><h2>List Jasa</h2></center>
                            <br>

                            <!--  duplicate field -->
                            <div class="input-group control-group after-add-more">
                            </div>

                            <!--  duplicate field -->
                            <div class="input-group total-reguler">
                            </div>

                            <center><button type="submit" class="button bkg-theme border-hover-theme color-white color-hover-theme" name="button">Simpan</button></center>

                    </div>
                </div>
                </div>

                    </div>
                </div>
                </div>
            </form>
    @endif

</div>
<!-- Content End -->
@endsection
@section('plugin')
<script type="text/javascript">

var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";

</script>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">

var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";
function getFormatDate(d){
    return d.getMonth()+1 + '/' + d.getDate() + '/' + d.getFullYear()
}
function hitung(id) {
    var qty = $('#qty'+id).val();
    var total_jasa = qty*$('#harga'+id).val();
    $('#total'+id).val(total_jasa);
    calculateSum();
}

function calculateSum() {
	var sum = 0;
	//iterate through each textboxes and add the values
	$(".total").each(function() {

		//add only if the value is number
		if(!isNaN(this.value) && this.value.length!=0) {
			sum += parseFloat(this.value);
		}

	});
	//.toFixed() method will roundoff the final sum to 2 decimal places
	$("#totalsemua").val(sum);
}
$(document).ready(function() {
        $(".total-reguler").html('');
        $('#list').html('');
        $('#harga').remove('');
        $('#timepicker').timepicker({
           timeFormat: 'HH:mm:ss',
           maxHour: 21,
           minHour: 7,
           maxMinutes: 30,
           startTime: new Date(0,0,0,7,0,0), // 3:00:00 PM - noon
           interval: 30 // 15 minutes
       });


        $("#daterange").keydown(function (event) {
            event.preventDefault();
        });
        if ($(this).val() == 'Paket') {
            mdTemp = new Date(),
            maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 30)));
        }
        else {
            mdTemp = new Date(),
            maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 2)));
        }
        var invalidDate = [
            @foreach($invalidDate as $key => $date)
            '{{$date->date}}',
            @endforeach
        ];

        @if(!isset($_GET['created_at']))
            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                minDate: maxDate,
                isInvalidDate: function(date) {
                    for (var ii = 0; ii < invalidDate.length; ii++){
                        if (date.format('YYYY-MM-DD') == invalidDate[ii]){
                          return true;
                        }
                      }
                }
            },
            function(chosen_date) {
              $('#daterange').val(chosen_date.format('YYYY-MM-DD'));
            });
        @endif
            $(".total-reguler").html('</div><div class="form-group column width-12"> <p class="text-muted m-b-15 f-s-12">Total</p> <input type="text" required class="form-fname form-element large input-default " placeholder="Total" name="final" id="totalsemua" readonly> </div>');
            $('#list').html('');
            $('.duplicate').remove();
            // $('#list').append('<div class="form-group" id="serve"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <select id="select_serve" class="form-fname form-element large serve select2" multiple name="serve[]" required style="height:42px" required> @foreach($serves as $serve) <option value="{{$serve->id}}">{{$serve->name}}</option> @endforeach </select> </div>');
            $('.select2').select2({
                placeholder: 'Pilih',
                allowClear: true
            });

            $(".serve").on("select2:unselect", function (e) {
                var lastSelectedId = e.params.data.id;
                var lastSelectedName = e.params.data.text;
                $('.nama_jasa'+lastSelectedId).remove();
                $('.harga_jasa'+lastSelectedId).remove();
                $('.jumlah_jasa'+lastSelectedId).remove();
                $('.total_jasa'+lastSelectedId).remove();
                calculateSum();
            });
});
function select_item(lastSelectedId) {
    $.ajax({
        url: "{{ url('page/jasa/') }}/" + lastSelectedId,
        type: "GET",
        dataType: "html",
        success: function (result) {
            var datas = JSON.parse(result);
            var duplicate = '<div class="duplicate form-group column width-2 nama_jasa'+datas.id+'" id="nama_jasa'+datas.id+'"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <input type="text" name="" value="'+datas.name+'" class="form-fname form-element large" disabled><input type="hidden" name="serve[]" value="'+datas.id+'" class="form-fname form-element large"> </div> <div class="duplicate form-group column width-2 harga_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Harga</p> <input type="number" name="harga[]" class="form-fname form-element large" id="harga'+datas.id+'" placeholder="Harga" required readonly value="'+datas.price+'"> </div> <div class="duplicate form-group column width-2 min'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Minimal Penyewaan</p> <input disabled type="number" class="form-fname form-element large" value="'+datas.min+'"> </div> <div class="duplicate form-group column width-2 stock'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Stok</p> <input type="number" class="form-fname form-element large" value="'+datas.stock+'" disabled> </div> <div class="duplicate form-group column width-2 jumlah_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" id="qty'+datas.id+'" name="qty[]" class="form-fname form-element large qty" min="'+datas.min+'" max="'+datas.stock+'" placeholder="Jumlah Dipesan" required onkeyup="hitung('+datas.id+')"> </div> <div class="duplicate form-group column width-2 total_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Total Harga</p> <input type="text" readonly name="total[]" class="form-fname form-element large total" placeholder="Total Harga" required value="" readonly id="total'+datas.id+'"> </div>';
            if (!document.getElementById('nama_jasa'+datas.id)) {
                $(".after-add-more").append(duplicate);
                var abc = $(".after-add-more");
                if(abc.length){
                    $('html, body').animate({
                        scrollTop: $("#hahahahaha").offset().top
                    }, 1000);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
                swal("Gagal!", "Silahkan coba lagi", "error");
        }
    });;
}
$('select[name="options"]').find('option:contains("Pakistan")').attr("selected",true);
</script>
@endsection
