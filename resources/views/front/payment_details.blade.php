@extends('front.layout')
@section('content')
<!-- Content -->
<style media="screen">
#clockdiv{
font-family: sans-serif;
color: #fff;
display: inline-block;
font-weight: 100;
text-align: center;
font-size: 30px;
}

#clockdiv > div{
padding: 10px;
border-radius: 3px;
background: #00BF96;
display: inline-block;
}

#clockdiv div > span{
padding: 15px;
border-radius: 3px;
background: #00816A;
display: inline-block;
}

.smalltext{
padding-top: 5px;
font-size: 16px;
}
</style>
<div class="content clearfix">

    <br><br>
    <div class="section-block pt-80 no-padding-bottom">
        <div class="row">
            <div class="class width-12">
                <h2 align="center">Pembayaran</h2>
            </div>
        </div>
    </div>

    <!-- Checkout -->
    <div class="section-block cart-overview">
        <div class="row">
            <div class="column width-12">
                <div class="payment-details box large">
                    <div class="tabs style-2">
                        <ul class="tab-nav">
                            <li class="active">
                                <a href="#tabs-1-pane-1">Pesanan</a>
                            </li>
                        </ul>
                        <div class="tab-panes">
                            <div id="tabs-1-pane-1" class="active animate">
                                <div class="tab-content">
                                    <div class="billing-form-container">
                                        <form class="billing-form" action="#" method="post" novalidate>
                                            <div class="row">
                                                <div class="column width-4">
                                                    <input type="text" readonly name="fname" value="Pemesan" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
                                                </div>
                                                <div class="column width-8">
                                                    <input type="text" readonly value="{{$data->rents->users->name}}" name="lname" class="form-lname form-element large" placeholder="Last Name" tabindex="2" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column width-4">
                                                    <input type="text" readonly name="fname" value="Kode Penyewaan" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
                                                </div>
                                                <div class="column width-8">
                                                    <input type="text" readonly value="{{$data->rents->code}}" name="lname" class="form-lname form-element large" placeholder="Last Name" tabindex="2" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column width-4">
                                                    <input type="text" readonly name="fname" value="Kode Pembayaran" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
                                                </div>
                                                <div class="column width-8">
                                                    <input type="text" readonly value="{{$data->code}}" name="lname" class="form-lname form-element large" placeholder="Last Name" tabindex="2" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="column width-4">
                                                    <input type="text" readonly name="fname" value="Tipe Penyewaan" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
                                                </div>
                                                <div class="column width-8">
                                                    <input type="text" readonly value="{{$data->rents->type}} @if($data->rents->type == 'Paket') {{$data->rents->types->name}} @endif" name="lname" class="form-lname form-element large" placeholder="Last Name" tabindex="2" required>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-12">
                <hr>
            </div>
        </div>
        <!-- Checkout End -->

        <!-- Order Overview -->
        <div class="row">
            <div class="column width-12">
                <h5 class="mb-30">Detail Pesanan</h5>
                <div class="cart-review">

                    @if($data->rents->type == 'Paket')
                    <table class="table non-responsive">
                        <thead>
                            <tr>
                                <th class="product-name">Nama Jasa</th>
                                <th class="product-subtotal">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->rents->details as $key => $detail)
                            <tr class="cart-item">
                                <td class="product-name">
                                    <a href="#" class="product-title">{{$detail->serves->name}}</a>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount">{{$detail->qty}} / {{$detail->serves->unit}}</span>
                                </td>
                            </tr>
                            @endforeach
                            <tr class="cart-order-total">
                                <th>Diskon</th>
                                <td><span class="amount">Rp {{number_format($data->rents->discount)}}</span></td>
                            </tr>
                            <tr class="cart-order-total">
                                <th>Total Harga</th>
                                <td><span class="amount">Rp {{number_format($data->rents->total-$data->rents->discount)}}</span></td>
                            </tr>

                            <tr class="cart-order-total">
                                <th>Dibayar</th>
                                <td><span class="amount">Rp {{number_format($data->where('status',1)->where('rent_id',$data->rent_id)->sum('total'))}}</span></td>
                            </tr>
                            <tr class="cart-order-total">
                                <th>Sisa</th>
                                <td><span class="amount">Rp {{number_format($data->rents->total - $data->rents->discount - $data->where('status',1)->where('rent_id',$data->rent_id)->sum('total'))}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                    @endif

                    @if($data->rents->type == 'Reguler')
                    <table class="table non-responsive">
                        <thead>
                            <tr>
                                <th class="product-name">Nama Jasa</th>
                                <th class="product-subtotal">Harga</th>
                                <th class="product-subtotal">Jumlah</th>
                                <th class="product-subtotal">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->rents->details as $key => $detail)
                            <tr class="cart-item">
                                <td class="product-name">
                                    <a href="#" class="product-title">{{$detail->serves->name}}</a>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount">{{$detail->serves->price}}</span>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount">{{$detail->qty}} / {{$detail->serves->unit}}</span>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount">{{$detail->total}}</span>
                                </td>
                            </tr>
                            @endforeach
                            <tr class="cart-order-total">
                                <th colspan="2"></th>
                                <th colspan="1">Total Harga</th>
                                <td><span class="amount">{{$data->rents->total}}</span></td>
                            </tr>
                            <tr class="cart-order-total">
                                <th colspan="2"></th>
                                <th colspan="1">Diskon</th>
                                <td><span class="amount">Rp {{number_format($data->rents->discount)}}</span></td>
                            </tr>
                            <tr class="cart-order-total">
                                <th colspan="2"></th>
                                <th colspan="1">Total Harga</th>
                                <td><span class="amount">Rp {{number_format($data->rents->total-$data->rents->discount)}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                    @endif


                </div>
                <div class="checkout-payment">
                    <h5 class="mb-30">Tipe Pembayaran</h5>
                    <table class="table non-responsive">
                        <tbody>
                            <tr class="payment-method-1">
                                <td>
                                    <input id="payment-method-1" class="form-element radio" name="payment_method" type="radio" readonly disabled @if($data->type == 'Tunai') checked @endif>
                                    <label for="payment-method-1" class="radio-label">Tunai / Lunas</label>
                                </td>
                            </tr>
                            <tr class="payment-method-2">
                                <td>
                                    <input id="payment-method-2" class="form-element radio" name="payment_method" type="radio" readonly disabled @if($data->type != 'Tunai') checked @endif>
                                    <label for="payment-method-2" class="radio-label">Dp</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="cart-actions center">
                        <div id="clockdiv">

                            <div class="smalltext">Sisa waktu pembayaran</div>
                            <br>
                            <br>
                            <div>
                                <span class="days"></span>
                                <div class="smalltext">Hari</div>
                            </div>
                            <div>
                                <span class="hours"></span>
                                <div class="smalltext">Jam</div>
                            </div>
                            <div>
                                <span class="minutes"></span>
                                <div class="smalltext">Menit</div>
                            </div>
                            <div>
                                <span class="seconds"></span>
                                <div class="smalltext">Detik</div>
                            </div>
                        </div>
                        <br>
                        <p id="demo"></p>

                        Silahkan transfer ke rekening Bca 3214124 a/n Wedding
                        <br>
                        Dengan jumlah
                        <br>
                        Rp {{number_format($data->total)}}
                        <br>
                        Dan sertakan bukti upload disini
                        <br>
                        <form method="post" action="{{url('/page/pembayaran/').'/'.$data->id}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="file" name="file" class="form-fname form-element large center" accept="image/*">
                            <button type="submit" name="button" class="button">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Order Overview End -->

</div>
<!-- Content End -->
@endsection
@section('plugin')

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
$('#pesan_baru').click( function() {
    $('#pemesanan_').fadeIn();
});
var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";

</script>
<script>
function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            // alert('a');
            // clearInterval(timeinterval);
            $.ajax({
                url: "{{ url('page/pembayaran/expired') }}/" + {{$data->id}} ,
                type: "get",
                dataType: "html",
                success: function (result) {
                    swal("Gagal!", "Pembayaran tidak dilakukan", "error", 20000, false);
                    window.location.replace('{{url ("/page/penyewaan")}}');
                }
            });
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date('{{$data->expired_at}}')));
initializeClock('clockdiv', deadline);
</script>
@endsection
</form>
