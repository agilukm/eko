@extends('front.layout')
@section('content')
<!-- Content -->
<div class="content clearfix">

    <!-- Fullscreen Slider Section -->
    <section class="section-block featured-media tm-slider-parallax-container">
        <div class="tm-slider-container full-width-slider" data-featured-slider data-parallax data-scale-under="960">
            <ul class="tms-slides">
                <li class="tms-slide" data-image data-as-bkg-image data-force-fit data-overlay-bkg-color="#000000" data-overlay-bkg-opacity="0.70">
                    <div class="tms-content">
                        <div class="tms-content-inner center v-align-middle">
                            <div class="row">
                                <div class="column width-12">
                                    <h1 class="tms-caption color-white title-xlarge mb-0"
                                        data-animate-in="preset:slideInUpShort;duration:900ms;delay:100ms;"
                                        data-no-scale
                                        >Cherariz Wedding</h1>
                                    <div class="clear"></div>
                                    <p class="tms-caption lead mb-20 color-grey-ultralight"
                                        data-animate-in="preset:slideInUpShort;duration:900ms;delay:200ms;"
                                        data-no-scale
                                        >A new level of wedding organization</p>
                                    <div class="clear"></div>
                                    <div class="tms-caption"
                                        data-animate-in="preset:slideInUpShort;duration:900ms;delay:400ms;"
                                        data-no-scale
                                    >
                                        <a href="#block-design" data-offset="-60" class="button medium border-grey-light bkg-hover-theme color-white color-hover-white text-uppercase scroll-link">Pre Weding Foto</a>
                                    </div>
                                    <div class="tms-caption"
                                        data-animate-in="preset:slideInUpShort;duration:900ms;delay:400ms;"
                                        data-no-scale
                                    >
                                        <a href="#heros" data-offset="-60" class="button medium border-grey-light bkg-hover-theme color-white color-hover-white text-uppercase scroll-link">Gallery</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img data-src="https://www.glentanar.co.uk/images/_crop1200X600/weddings.jpg" src="https://www.glentanar.co.uk/images/_crop1200X600/weddings.jpg" alt=""/>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fullscreen Slider Section End -->

    <!-- Feature Section 2 -->
    <section id="block-design" class="section-block feature-2">
        <div class="row flex">
            <div class="column width-6 push-6">
                <div class="feature-image mb-mobile-50">
                    <div class="feature-image-inner center-on-mobile horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.6">
                        <img src="{{ URL('/') }}/front/images/eko8.jpg" alt="Sartre HTML Shop Grid Layou" title="Sartre Multipurpose HTML Template Shop Grid Layout" />
                    </div>
                </div>
            </div>
            <div class="column width-5 pull-6 pull-1">
                <div class="feature-content">
                    <div class="feature-content-inner horizon center-on-mobile" data-animate-in="preset:slideInUpShort;duration:900ms;delay:300ms;" data-threshold="0.6">
                        <h2 class="mb-30">Pre Wedding Foto</h2>
                        <p class="lead">Build truly stunning layouts quickly by mixing pre-designed content blocks from different layouts. Simple!</p>
                        <p>Sartre comes with some 70+ page layouts that offer unique and beautifully design blocks that look great on desktop, tablet and mobile. Sartre now also includes fully functional onepage layouts! The possibilities are endless.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Feature Section 2 End -->

    <!-- Feature Section 2 -->
    <section class="section-block bkg-charcoal feature-2 right">
        <div class="row flex">
            <div class="column width-6">
                <div class="feature-image mb-mobile-50">
                    <div class="feature-image-inner center-on-mobile horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.6">
                        <img src="{{ URL('/') }}/front/images/eko7.jpg" alt="Sartre HTML Off Canvas Navigation" title="Sartre Multipurpose HTML Off Canvas Navigation" />
                    </div>
                </div>
            </div>
            <div class="column width-5 push-1">
                <div class="feature-content">
                    <div class="feature-content-inner left center-on-mobile horizon" data-animate-in="preset:slideInUpShort;duration:900ms;delay:300ms;" data-threshold="0.6">
                        <h2 class="mb-30 color-white">Pre Wedding</h2>
                        <p class="lead color-grey-light">Sartre offers several navgiation types that are functional yet beautiful.</p>
                        <p class="color-grey-light">With multiple navigation types and a variety of ways to style headers, main menus, and auxiliary navigation, you can create a totally unique experience for your users.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Feature Section 2 End -->

    <!-- Hero Grid -->
    <div id="heros" class="section-block pt-80 no-padding-bottom bkg-grey-ultralight">
        <div class="row">
            <div class="column width-12">
                <h2 class="mb-60">Gallery</h2>
            </div>
        </div>
    </div>
    <div class="section-block grid-container fade-in-progressively no-padding-top bkg-grey-ultralight" data-layout-mode="masonry" data-animate-filter-duration="700" data-animate-resize data-animate-resize-duration="0">
        <div class="row">
            <div class="column width-12">
                <div class="row grid content-grid-3">
                    <div class="grid-item grid-sizer">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-background-video.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko1.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <span class="icon-plus"></span>
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider w/ HTML5 Bkg Video</h3> -->
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-background-video-vimeo.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko2.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <span class="icon-plus"></span>
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider w/ Vimeo Bkg Video <span class="label bkg-theme color-white pull-right">New</span></h3> -->
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-background-video-youtube.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko3.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <span class="icon-plus"></span>
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider w/ YouTube Bkg Video <span class="label bkg-theme color-white pull-right">New</span></h3> -->
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-video.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko4.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <!-- <span class="icon-plus"></span> -->
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider w/ Video</h3> -->
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-caption-showdown.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko5.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <!-- <span class="icon-plus"></span> -->
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider Caption Showdown</h3> -->
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                            <!-- <a class="overlay-link" href="index-slider-caption-showdown.html"> -->
                                <img src="{{ URL('/') }}/front/images/eko6.jpg" alt=""/>
                                <span class="overlay-info">
                                    <span>
                                        <span>
                                            <!-- <span class="icon-plus"></span> -->
                                        </span>
                                    </span>
                                </span>
                            <!-- </a> -->
                        </div>
                        <div class="item-description with-background">
                            <!-- <h3 class="project-title">Slider Caption Showdown</h3> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Grid End -->

    <!-- Feature Column Section -->
    <div id="discover" class="section-block replicable-content">
        <div class="row">
            <div class="column width-8 offset-2 center">
                <h2 class="mb-10">Express Yourself With Sartre</h2>
                <p class="lead mb-60">Sartre provides you with the tools you need to create a unique website.</p>
            </div>
        </div>
        <div class="row flex">
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small mb-50 left">
                        <span class="feature-icon icon-map color-charcoal"></span>
                        <div class="feature-text">
                            <h3>24 Variations</h3>
                            <p>Sartre is focused around 8 unique concepts with 3 variations each. Sartre has got you covered whether you are a startup or established business.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small left">
                        <span class="feature-icon icon-add-to-list color-charcoal"></span>
                        <div class="feature-text">
                            <h3>20+ Components</h3>
                            <p>Make any design stand out by using a combination of the 20+ components that come with Sartre. Accordions, tabs, buttons and much more.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small mb-50 left">
                        <span class="feature-icon icon-power-plug color-charcoal"></span>
                        <div class="feature-text">
                            <h3>8+ Plugins</h3>
                            <p>As with all our templates, Sartre comes with 8 premium in-house developed plugins, which means that when there's an update, you'll get it.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small left">
                        <span class="feature-icon icon-grid color-charcoal"></span>
                        <div class="feature-text">
                            <h3>Block Based</h3>
                            <p>Sartre, like all our templates, is built using a block based, responsive framework that makes designing easier. Move blocks around and get a new design.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small mb-50 left">
                        <span class="feature-icon icon-credit color-charcoal"></span>
                        <div class="feature-text">
                            <h3>Ultimate Startup Pack</h3>
                            <p>Sartre is a great starting point for startups that need a quick and simple solution when it comes to creating a site. Save both time &amp; money.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column width-4">
                <div class="box small bkg-white">
                    <div class="feature-column small left">
                        <span class="feature-icon icon-infinity color-charcoal"></span>
                        <div class="feature-text">
                            <h3>Infinite Possibilities</h3>
                            <p>With a range of pre-design content blocks, components and plugins, Sarte provides you with an unlimited number of possibilities.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Feature Column Section End -->

</div>
<!-- Content End -->
@endsection
@section('plugin')
@endsection
