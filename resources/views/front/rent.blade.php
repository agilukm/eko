@extends('front.layout')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@section('content')
<!-- Content -->
<form action="{{url('page/penyewaan/simpan')}}" method="post">
<div class="content clearfix">
    <br><br>
    <div class="section-block pt-80 no-padding-bottom">
        <div class="row">
            <div class="class width-12">
                <h2 align="center">Penyewaan</h2>
            </div>
            <div class="column width-12">
                <hr>
            </div>
        </div>
    </div>

    <!-- Tabs Style 1 -->
    <div class="section-block pt-60 pb-0">
        <div class="row">
            <div class="column width-12">

                <table id="myTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width='1%'>No.</th>
                            <th>Kode Pemesanan</th>
                            <th>Tanggal Pemesanan</th>
                            <th>Tanggal Pelaksanaan</th>
                            <th>Tipe</th>
                            <th>Diskon</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th align="center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key => $value): ?>
                            <tr class="{{$key}}">
                                <td>{{$key+1}}</td>
                                <td>{{$value->code}}</td>
                                <td>{{$value->created_at}}</td>
                                <td>{{$value->date}}</td>
                                <td>{{$value->type}}</td>
                                <td>{{number_format($value->discount)}}</td>
                                <td>{{number_format($value->total-$value->discount)}}</td>
                                <td>{{$value->statusLabel()}}</td>
                                <td>
                                    @if ($value->status == 0 || $value->status == 2 || $value->status == 3)
                                    <center>
                                        <a href="{{url ('page/pembayaran/').'/'.$value->payments->last()->id}}"><button type="button" class="conf btn btn-danger" style="width:150px;" value="">Bayar</button></a>
                                    @elseif($value->status != 7 && $value->status != 8)
                                    <center>
                                        <a href="{{url ('page/penyewaan/invoice/').'/'.$value->id}}" target="_blank"><button type="button" class="conf button btn btn-danger" style="width:150px;" value="">Bukti Transaksi</button></a>
                                    @endif
                                    <center>
                                    <a data-content="inline" data-aux-classes="tml-message-modal" data-toolbar="" data-modal-mode data-modal-width="600" style="width:150px;" data-lightbox-animation="fadeIn" href="#message-modal{{$value->id}}" class="conf lightbox-link button bkg-theme bkg-hover-theme color-white color-hover-white"><center>Detail</center></a>
                                </center>
                                    <!-- Search Modal End -->
                    				<div id="message-modal{{$value->id}}" class="section-block pt-50 pb-20 background-none hide">

                    					<!-- Message -->
                    					<div class="row">
                    						<div class="column width-12 center">
                                                <p> @if($value->type == 'Paket') {{$value->types->name}} @else {{$value->type}} @endif</p>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <th>No</th>
                                                        <th>Nama Jasa</th>
                                                        <th>Jumlah</th>
                                                        <th>Satuan</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($value->details as $key => $details): ?>
                                                        <tr>
                                                            <td>{{++$key}}</td>
                                                            <td>{{$details->serves->name}}</td>
                                                            <td>{{$details->qty}}</td>
                                                            <td>{{number_format($details->serves->price)}}/{{$details->serves->unit}}</td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>

                                                </table>
                    						</div>
                    					</div>
                    					<!-- Message End -->

                    				</div>
                    				<!-- Search Modal End -->

                                </td>
                            </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
                <center><button type="button" name="button" class="button bkg-facebook border-hover-facebook color-white color-hover-facebook" id="pesan_baru">Pemesanan Baru</button></center>
                <br>
                <br>
                <div id="pemesanan_" style="display:none">
                    <div class="card-body">
                    <div class="basic-form">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-fname form-element large" required name="user_id" required value="{{Auth::user()->id}}">

                            <div class="form-group">
                                <p class="text-muted m-b-15 f-s-12">Tipe</p>
                                <select class="form-fname form-element large" required name="type" id="tipe" required>
                                    <option value="">Pilih</option>
                                    <option value="Paket">Paket</option>
                                    <option value="Reguler">Reguler</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <p class="text-muted m-b-15 f-s-12">Alamat Penyewaan</p>
                                <input type="text" class="form-fname form-element large" required name="address" required>
                            </div>
                            <div class="" id="datetimehe">

                            </div>
                            <div id="list">

                            </div>


                            <center><h2>List Jasa</h2></center>
                            <br>

                            <!--  duplicate field -->
                            <div class="input-group control-group after-add-more">
                            </div>

                            <!--  duplicate field -->
                            <div class="input-group total-reguler">
                            </div>

                            <center><button type="submit" class="button bkg-theme border-hover-theme color-white color-hover-theme" name="button">Simpan</button></center>

                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Tabs Style 1 End -->



</div>
<!-- Content End -->
@endsection
@section('plugin')

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
$('#pesan_baru').click( function() {
    $('#pemesanan_').fadeIn();
});
var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";
function getFormatDate(d){
    return d.getMonth()+1 + '/' + d.getDate() + '/' + d.getFullYear()
}
function hitung(id) {
    var qty = $('#qty'+id).val();
    var total_jasa = qty*$('#harga'+id).val();
    $('#total'+id).val(total_jasa);
    calculateSum();
}

function calculateSum() {
	var sum = 0;
	//iterate through each textboxes and add the values
	$(".total").each(function() {

		//add only if the value is number
		if(!isNaN(this.value) && this.value.length!=0) {
			sum += parseFloat(this.value);
		}

	});
	//.toFixed() method will roundoff the final sum to 2 decimal places
	$("#totalsemua").val(sum);
}
$(document).ready(function() {





$("#tipe").change( function() {
    $(".total-reguler").html('');
    $('#list').html('');
    $('#harga').remove('');
    $('#datetimehe').html('');
    $('#datetimehe').html(' <div class="form-group"> <p class="text-muted m-b-15 f-s-12">Tanggal</p> <input type="text" id="daterange" autocomplete="off" required class="form-fname form-element large " name="date"> </div> <div class="form-group"> <p class="text-muted m-b-15 f-s-12">Jam</p> <input type="text" id="timepicker" autocomplete="off" required class="form-fname form-element large " name="time"> </div>');
    $('#timepicker').timepicker({
       timeFormat: 'HH:mm:ss',
       maxHour: 21,
       minHour: 7,
       maxMinutes: 30,
       startTime: new Date(0,0,0,7,0,0), // 3:00:00 PM - noon
       interval: 30 // 15 minutes
   });


    $("#daterange").keydown(function (event) {
        event.preventDefault();
    });
    if ($(this).val() == 'Paket') {
        mdTemp = new Date(),
        maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 30)));
    }
    else {
        mdTemp = new Date(),
        maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 2)));
    }
    var invalidDate = [
        @foreach($invalidDate as $key => $date)
        '{{$date->date}}',
        @endforeach
    ];

    @if(!isset($_GET['created_at']))
        $('#daterange').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: maxDate,
            isInvalidDate: function(date) {
                for (var ii = 0; ii < invalidDate.length; ii++){
                    if (date.format('YYYY-MM-DD') == invalidDate[ii]){
                      return true;
                    }
                  }
            }
        },
        function(chosen_date) {
          $('#daterange').val(chosen_date.format('YYYY-MM-DD'));
        });
    @endif


    if ($(this).val() == 'Paket') {
        $('#list').html('');
        $('.duplicate').remove();
        $('#list').append('<div class="form-group" id="package"> <p class="text-muted m-b-15 f-s-12">Paket</p> <select id="select_package" class="form-fname form-element large package" name="package" required> <option value="">Pilih</option> @foreach($packages as $package) <option value="{{$package->id}}">{{$package->name}}</option> @endforeach </select> </div>');
        $('.select2').select2();
    }
    else if ($(this).val() == 'Reguler') {
        $(".total-reguler").html('</div><div class="form-group column width-12"> <p class="text-muted m-b-15 f-s-12">Total</p> <input type="text" required class="form-fname form-element large input-default " placeholder="Total" name="final" id="totalsemua" readonly> </div>');
        $('#list').html('');
        $('.duplicate').remove();
        $('#list').append('<div class="form-group" id="serve"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <select id="select_serve" class="form-fname form-element large serve select2" multiple name="serve[]" required style="height:42px" required> @foreach($serves as $serve) <option value="{{$serve->id}}">{{$serve->name}}</option> @endforeach </select> </div>');
        $('.select2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });

        $(".serve").on("select2:select", function (e) {
            var lastSelectedId = e.params.data.id;
            var lastSelectedName = e.params.data.text;
            $.ajax({
                url: "{{ url('page/jasa/') }}/" + lastSelectedId,
                type: "GET",
                dataType: "html",
                success: function (result) {
                    var datas = JSON.parse(result);
                    var duplicate = '<div class="duplicate form-group column width-2 nama_jasa'+datas.id+'" id="nama_jasa'+datas.id+'"> <p class="text-muted m-b-15 f-s-12">Jasa</p> <input type="text" name="" value="'+datas.name+'" class="form-fname form-element large" disabled><input type="hidden" name="serve[]" value="'+datas.id+'" class="form-fname form-element large"> </div> <div class="duplicate form-group column width-2 harga_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Harga</p> <input type="number" name="harga[]" class="form-fname form-element large" id="harga'+datas.id+'" placeholder="Harga" required readonly value="'+datas.price+'"> </div> <div class="duplicate form-group column width-2 min'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Minimal Penyewaan</p> <input disabled type="number" class="form-fname form-element large" value="'+datas.min+'"> </div> <div class="duplicate form-group column width-2 stock'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Stok</p> <input type="number" class="form-fname form-element large" value="'+datas.stock+'" disabled> </div> <div class="duplicate form-group column width-2 jumlah_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" id="qty'+datas.id+'" name="qty[]" class="form-fname form-element large qty" min="'+datas.min+'" max="'+datas.stock+'" placeholder="Jumlah Dipesan" required onkeyup="hitung('+datas.id+')"> </div> <div class="duplicate form-group column width-2 total_jasa'+datas.id+'" id=""> <p class="text-muted m-b-15 f-s-12">Total Harga</p> <input type="text" readonly name="total[]" class="form-fname form-element large total" placeholder="Total Harga" required value="" readonly id="total'+datas.id+'"> </div>';
                    $(".after-add-more").append(duplicate);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Silahkan coba lagi", "error");
                }
            });
        });

        $(".serve").on("select2:unselect", function (e) {
            var lastSelectedId = e.params.data.id;
            var lastSelectedName = e.params.data.text;
            $('.nama_jasa'+lastSelectedId).remove();
            $('.harga_jasa'+lastSelectedId).remove();
            $('.jumlah_jasa'+lastSelectedId).remove();
            $('.total_jasa'+lastSelectedId).remove();
            $('.min'+lastSelectedId).remove();
            $('.stock'+lastSelectedId).remove();
            calculateSum();
        });

    }
    else {
        $('#list').html('');
        $('.duplicate').remove();
    }

    $("#select_package").change( function() {
        $('#harga').remove('');
        $('.after-add-more').html('');
        $.ajax({
            url: "{{ url('page/paket/') }}/" + $(this).val() + "/details",
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                $("#list").append(' <div class="form-group" id="harga"> <p class="text-muted m-b-15 f-s-12">Harga</p> <input type="text" required class="form-fname form-element large input-default " name="" readonly value="'+format_number(datas.data.price, "Rp")+'"> <input type="hidden" required class="form-fname form-element large input-default " name="final" readonly value="'+datas.data.price+'" id="harga-final"> </div>');

                $.each(datas.data.details, function(k,v){
                    $('.after-add-more').append('<div class="duplicate form-group column width-6 nama_jasa" id=""><p class="text-muted m-b-15 f-s-12">Jasa</p><input type="hidden" name="serve[]" value="'+datas.data.details[k].serve.id+'"><input type="text" name="" value="'+datas.data.details[k].serve.name+'" class="form-fname form-element large" disabled> </div> <div class="duplicate form-group column width-6 jumlah_jasa" id=""> <p class="text-muted m-b-15 f-s-12">Jumlah</p> <input type="number" name="qty[]" class="form-fname form-element large" value="'+datas.data.details[k].total_unit+'" placeholder="Jumlah" required readonly><input type="hidden" value="0" name="total[]"></div> </div>');
                });
                $('#list').append('<div class="form-group" id="pembayaran"> <p class="text-muted m-b-15 f-s-12">Pembayaran</p> <select class="form-fname form-element large package" name="payment_type" required id="payment_type"> <option value="">Pilih</option> <option value="Dp">Dp</option><option value="Tunai">Tunai</option>  </select> </div></div><div class="after-payment"></div>');
                $('#payment_type').change( function() {
                    if ($(this).val() == 'Dp') {
                        var harga_final = $('#harga-final').val();
                        var dp = harga_final*0.5;
                        $('.after-payment').html('<div class="form-group id="value-payment"><p class="text-muted m-b-15 f-s-12">Uang Muka</p><input type="number" class="form-fname form-element large" required name="dp" min="'+dp+'" max="'+harga_final+'"></div>');
                    } else {
                        $('.after-payment').html('');
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
    });
});
});
</script>
@endsection
</form>
