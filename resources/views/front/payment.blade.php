@extends('front.layout')
@section('content')
<!-- Content -->
<form action="{{url('page/penyewaan/simpan')}}" method="post">
<div class="content clearfix">
    <br><br>
    <div class="section-block pt-80 no-padding-bottom">
        <div class="row">
            <div class="class width-12">
                <h2 align="center">Pembayaran</h2>
            </div>
            <div class="column width-12">
                <hr>
            </div>
        </div>
    </div>

    <!-- Tabs Style 1 -->
    <div class="section-block pt-60 pb-0">
        <div class="row">
            <div class="column width-12">

                <table id="myTable" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width='1%'>No.</th>
                            <th>Kode Pemesanan</th>
                            <th>Kode Pembayaran</th>
                            <th>Tanggal</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $number = 1; @endphp
                        <?php foreach ($data as $key => $value): ?>
                            <?php if ($value->rents->users->id == Auth::user()->id): ?>

                            <tr class="{{$key}}">
                                <td>{{$number++}}</td>
                                <td>{{$value->rents->code}}</td>
                                <td>{{$value->code}}</td>
                                <td>{{$value->created_at}}</td>
                                <td>{{number_format($value->total)}}</td>
                                <td>{{$value->statusLabel()}}</td>
                                <td>
                                    <?php if ($value->status == 3 || $value->status == 2): ?>
                                        <a href="{{url ('page/pembayaran/').'/'.$value->id}}"><button type="button" class="conf btn btn-danger" value="">Bayar</button></a>
                                    <?php endif; ?>
                                    <?php if ($value->status != 3 && $value->status != 7): ?>
                                        <a href="{{$value->file}}" target="_blank"><button type="button" class="conf btn btn-danger" value="">Bukti Pembayaran</button></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Tabs Style 1 End -->



</div>
<!-- Content End -->
@endsection
@section('plugin')

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
$('#pesan_baru').click( function() {
    $('#pemesanan_').fadeIn();
});
var myElement = document.querySelector(".header");
myElement.style.backgroundColor = "black";

</script>
@endsection
</form>
