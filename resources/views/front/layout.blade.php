<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="Your-Title-Here" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="Your-Image-Url" />
	<meta property="og:description" content="Your-Page-Description" />
	<title>Cherariz Wedding</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/theme-mountain-favicon.ico">

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700%7CLato:300,400,700' rel='stylesheet' type='text/css'>

	<!-- Css -->
	<link rel="stylesheet" href="{{ URL('/') }}/front/css/core.min.css" />
	<link rel="stylesheet" href="{{ URL('/') }}/front/css/skin.css" />
    <link href="{{url('/')}}/admin/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="shop home-page">

	<div class="wrapper">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-fixed header-fixed-on-mobile header-transparent" data-bkg-threshold="100" data-compact-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="logo">
								<div class="logo-inner">
									<h2 style="color:black; text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;">Cherariz Wedding</h2>
								</div>
							</div>
							<nav class="navigation nav-block secondary-navigation nav-right">
								<ul>
                                    @if(Auth::check() && Auth::user()->roles == 0 && Auth::user()->owner == 0)
                                    <li>
										<!-- Dropdown Login Module -->
										<div class="dropdown">
											<a href="{{route ('logout')}}" onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();" class="nav-icon search button no-page-fade">Logout</a>
                                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                              @csrf
                                                          </form>
										</div>
									</li>
                                    @endif
                                    @if(!Auth::check() || Auth::user()->roles != 0 || Auth::user()->owner != 0)
									<li>
										<!-- Dropdown Login Module -->
										<div class="dropdown">
											<a href="#" class="nav-icon search button no-page-fade">Login</a>
											<div class="dropdown-list custom-content cart-overview" style="width:40rem">
												<div class="search-form-container site-search">
													<form action="{{ route('login') }}" method="post">
                                                        @csrf
														<div class="row">
															<div class="column width-12">
																<div class="field-wrapper">
                                                                    Email
																	<input type="text" required name="email" class="form-search form-element no-margin-bottom" placeholder="types &amp; hit enter...">
																	<span class="border"></span>
																</div>
																<div class="field-wrapper">
                                                                    Password
																	<input type="password"  required name="password" class="form-search form-element no-margin-bottom" placeholder="types &amp; hit enter...">
																	<span class="border"></span>
																</div>
                                                                <br>
                                                                <center>
                                                                    <input type="submit" name="" value="Login" class="button bkg-theme bkg-hover-theme color-white color-hover-white hard-shadow" value="Login">
                                                                </center>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>
									<li>
										<!-- Dropdown Register Module -->
										<div class="dropdown">
											<a href="#" class="nav-icon search button no-page-fade">Register</a>
											<div class="dropdown-list custom-content cart-overview" style="width:40rem">
												<div class="search-form-container site-search">
													<form action="{{ route('register') }}" method="post" >
                                                        @csrf
														<div class="row">
															<div class="column width-12">
																<div class="field-wrapper">
                                                                    Nama
																	<input type="text" name="name" required class="form-search form-element no-margin-bottom" placeholder="Nama Lengkap">
																	<span class="border"></span>
																</div>
																<div class="field-wrapper">
                                                                    Email
																	<input type="email" name="email" required class="form-search form-element no-margin-bottom" placeholder="Email">
																	<span class="border"></span>
																</div>
																<div class="field-wrapper">
                                                                    Alamat
																	<input type="text" name="address" required class="form-search form-element no-margin-bottom" placeholder="Alamat">
																	<span class="border"></span>
																</div>
																<div class="field-wrapper">
                                                                    No. Hp
																	<input type="number" name="phone" required class="form-search form-element no-margin-bottom" placeholder="No. Hp">
																	<span class="border"></span>
																</div>
																<div class="field-wrapper">
                                                                    Password
																	<input type="password" name="password" class="form-search form-element no-margin-bottom" placeholder="Password">
																	<span class="border"></span>
																</div>
                                                                <br>
                                                                <center>
                                                                    <input type="submit" name="" value="Daftar" class="button bkg-theme bkg-hover-theme color-white color-hover-white hard-shadow" value="Login">
                                                                </center>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>
                                    @endif
								</ul>
							</nav>
							<nav class="navigation nav-block primary-navigation nav-right">
								<ul>
									<li> <a href="{{ url('page')}}">Home</a> </li>
									<li> <a href="{{ url('page/jasa')}}">Jasa</a> </li>
									<li> <a href="{{ url('page/paket')}}">Paket</a> </li>
                                    @if(Auth::check() && Auth::user()->roles == 0 && Auth::user()->owner == 0)
									<li> <a href="{{ url('page/penyewaan')}}">Penyewaan</a> </li>
									<li> <a href="{{ url('page/pembayaran')}}">Pembayaran</a> </li>
                                    @endif
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

@yield('content')

			<!-- Footer -->
			<footer class="footer">
				<div class="footer-top">
					<div class="row flex">
						<div class="column width-6 offset-3 center">
							<div class="widget">
								<h4 class="widget-title">Handcrafted by ThememMountain</h4>
								<ul class="social-list list-horizontal">
									<li><a href="#"><span class="icon-twitter-with-circle medium"></span></a></li>
									<li><a href="#"><span class="icon-facebook-with-circle medium"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="row">
						<div class="column width-12">
							<div class="footer-bottom-inner">
								<p class="copyright pull-left">&copy; ThemeMountain. All Rights Reserved.</p>
								<a href="#" class="scroll-to-top pull-right">Top</a>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- Footer End -->

		</div>
	</div>
	<!-- Js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?v=3"></script>
	<script src="{{ URL('/') }}/front/js/timber.master.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
	<script src="{{ url('/')}}/admin/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    function format_number(n, currency) {
        return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }
    $(document).ready( function () {
        $('.select2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });
		// swal("Behasil!", "asd", "success", 3000, false);
    });
    </script>
	@if(session()->has('message'))
	<script type="text/javascript">
		swal("Behasil!", "{{ session()->get('message') }}", "success", 3000, false);
	</script>
	@endif
	@if(session()->has('error'))
	<script type="text/javascript">
		swal("Gagal!", "{{ session()->get('error') }}", "error", 3000, false);
	</script>
	@endif
	@if(count($errors) > 0)
	<script type="text/javascript">
		swal("Gagal!", "@foreach ($errors->all() as $error){{ $error }}@endforeach", "error", 3000, false);
	</script>
    @endif

	@if($errors->any())
	<script type="text/javascript">
		$(window).on('load',function(){
			$('#errormodal').modal('show');
		});
	</script>
	@endif
@yield('plugin')
</body>
</html>
