<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'username' => 'admin@admin.com',
                'password' => bcrypt('admin'),
                'roles' => '1',
                'owner' => '0'
            ],
            [
                'name' => 'pemilik',
                'email' => 'pemilik@pemilik.com',
                'username' => 'pemilik@pemilik.com',
                'password' => bcrypt('pemilik'),
                'roles' => '1',
                'owner' => '1'
            ],
            [
                'name' => 'Pelanggan',
                'email' => 'pelanggan@pelanggan.com',
                'username' => 'pelanggan@pelanggan.com',
                'password' => bcrypt('pelanggan'),
                'roles' => '0',
                'owner' => '0'
            ]
        ]);
        DB::table('serves')->insert([
            [
                'name' => 'test',
                'price' => '100000',
                'unit' => 'orang',
                'fileurl' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGD_EbSSw5JzEa-m9hG5AV3tDqdA94aK2JjWX70fzQDFyuuXqv',
                'type' => 'url'
            ],
            [
                'name' => 'test2',
                'price' => '100000',
                'unit' => 'orang',
                'fileurl' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGD_EbSSw5JzEa-m9hG5AV3tDqdA94aK2JjWX70fzQDFyuuXqv',
                'type' => 'url' 
            ],
        ]);
        DB::table('packages')->insert([
            [
                'name' => 'Mawar',
                'price' => '100000',
            ]
        ]);
        DB::table('package_details')->insert([
            [
                'package_id' => '1',
                'serve_id' => '1',
                'total_unit' => '4',
            ],
            [
                'package_id' => '1',
                'serve_id' => '2',
                'total_unit' => '2',
            ],
        ]);
    }
}
