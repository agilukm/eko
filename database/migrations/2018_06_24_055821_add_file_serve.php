<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileServe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serves', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->string('fileurl')->nullable();
            $table->string('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serves', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('fileurl');
            $table->dropColumn('type');
        });
    }
}
