<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTotalTableRents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rents', function (Blueprint $table) {
            $table->integer('total');
            $table->integer('status')->default('0');
            $table->integer('source_id')->nullable();
            });

        

        Schema::table('payments', function (Blueprint $table) {
            $table->integer('status')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rents', function (Blueprint $table) {
            $table->dropIfExists('total');
            $table->dropIfExists('status');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->dropIfExists('status');
        });
    }
}
