<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStockAndTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rents', function (Blueprint $table) {
            $table->dateTime('date')->change();
        });
        Schema::table('serves', function (Blueprint $table) {
            $table->integer('stock')->nullable();
            $table->integer('min')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serves', function (Blueprint $table) {
            $table->dropColumn('stock');
        });
    }
}
